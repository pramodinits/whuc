<?php
include SITE_ROOT . 'views/regHeader.php';
?>
<style>
    label[for="reg[participate_in]"],
    label[for="reg[participate_as]"],
    label[for="reg[gender]"]{
        position: absolute;
        top: 75%;
        text-align: left;
        color: #E42C3E !important;
    }

</style>
<div class="clearfix"></div>
<section role="main" class="content-body">
    <!--    <header class="page-header">
            <h2 class="panel-title">SSTA Users</h2>
        </header>-->
    <div class="panel-body">
        <div id="msg" class="btn btn-block btn-xs" style="display: none;"></div>
        <div class="row">
            <form class="form-horizontal form-bordered" name="regForm" id="regForm"  style="float: left;" action="<?= base_url('user/insertregUser/'); ?>" method="post" enctype="multipart/form-data" onsubmit="return AsyncUpload.submitForm(this, formvalidation, calbackFun);">
                <input type="hidden" name="id_user" value="<?php if (!empty($res['id_user'])) echo @$res['id_user'] ?>" id="id_user" />
                <input type="hidden" name="reg[flag]" value="2" id="flag" />
                <div class="col-md-6" style="padding-left: 10%;">
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Participate In<span>*</span></label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="participate_in" name="reg[participate_in]" value="Europe" /> 
                                        <br>
                                        <label for="participate_in">Europe</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="participate_in" name="reg[participate_in]" value="Africa" /> 
                                        <br>
                                        <label for="participate_in">Africa</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="participate_in" name="reg[participate_in]" value="South Asia" /> 
                                        <br>
                                        <label for="participate_in">South Asia</label>
                                    </li>
									<li>
                                        <input type="radio" id="participate_in" name="reg[participate_in]" value="Arabia" /> 
                                        <br>
                                        <label for="participate_in">Arabia</label>
                                    </li>
									<li>
                                        <input type="radio" id="participate_in" name="reg[participate_in]" value="South East Asia" /> 
                                        <br>
                                        <label for="participate_in">South East Asia</label>
                                    </li>
                                </ul>
                            </div>
                            <label for="participate_in" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Participate As<span>*</span></label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="participate_as" name="reg[participate_as]" value="Visitor" /> 
                                        <br>
                                        <label for="participate_as">Visitor</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="participate_as" name="reg[participate_as]" value="Exhibitor" /> 
                                        <br>
                                        <label for="participate_as">Exhibitor</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="participate_as" name="reg[participate_as]" value="Sponsor" /> 
                                        <br>
                                        <label for="participate_as">Sponsor</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Name Of Company<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[company_name]" value="" id="company_name" class="form-control" />
                            <label for="company_name" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Contact Full Name<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[contact_name]" value="" id="contact_name" class="form-control" />
                            <label for="contact_name" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Job Title<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[job_title]" value="" id="job_title" class="form-control" />
                            <label for="job_title" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Office Tel<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[office_telephone]" value="" id="office_telephone" class="form-control" />
                            <label for="office_telephone" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Mobile / Cellphone<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[mobile_no]" value="" id="mobile_no" class="form-control" />
                            <label for="mobile_no" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Email<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[email]" value="" id="email" class="form-control" />
                            <label for="email" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Gender<span>*</span></label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="gender" name="reg[gender]" value="1" /> 
                                        <br>
                                        <label for="gender">Male</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="gender" name="reg[gender]" value="2" /> 
                                        <br>
                                        <label for="gender">Female</label>
                                    </li>
                                </ul>
                            </div>
                            <label for="gender" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">Date of Birth<span>*</span></label>
                        <div class="col-sm-9">
                              <input type="date" name="dob" value="" id="dob" class="form-control"  /><span class="text-sm">ex. dd/mm/yyyy</span><br>
                            <label for="dob" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">Nature Of Company Business<span>*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="reg[company_business_nature]" value="" id="company_business_nature" class="form-control" />
                            <label for="company_business_nature" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">How many Hajj Pilgrims Do You Take Annually?</label>
                        <div class="col-sm-9">
                            <input type="text" name="hajj_pilgrims_annually" value="" id="hajj_pilgrims_annually" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">Would You Like to become a member of the World Hajj Umrah Organisers Association?</label>
                        <div class="col-sm-9">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="hajj_umrah_organisers" name="reg[hajj_umrah_organisers_assocciation_membership]" value="1" /> 
                                        <br>
                                        <label for="hajj_umrah_organisers">Yes</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="hajj_umrah_organisers" name="reg[hajj_umrah_organisers_assocciation_membership]" value="2" /> 
                                        <br>
                                        <label for="hajj_umrah_organisers">No</label>
                                    </li>
                                </ul>
                            </div>
                            <label for="hajj_umrah_organisers" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">Would You Like to become a member of the British Hajj and Umrah Council?</label>
                        <div class="col-sm-9">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="british_hajj_umrah" name="reg[british_hajj_umrah_council_membership]" value="1" /> 
                                        <br>
                                        <label for="british_hajj_umrah">Yes</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="british_hajj_umrah" name="reg[british_hajj_umrah_council_membership]" value="2" /> 
                                        <br>
                                        <label for="british_hajj_umrah">No</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">Would You Like to become a member of the National Pilgrimage Organisers Association?</label>
                        <div class="col-sm-9">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="national_pilgrimage" name="reg[national_pilgrimage_organisers_association]" value="1" /> 
                                        <br>
                                        <label for="national_pilgrimage">Yes</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="national_pilgrimage" name="reg[national_pilgrimage_organisers_association]" value="2" /> 
                                        <br>
                                        <label for="national_pilgrimage">No</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Preferred Payment Option</label>
                        <div class="col-sm-9">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="payment_option" name="reg[payment_option]" value="Bank Transfer" /> 
                                        <br>
                                        <label for="payment_option">Bank Transfer</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="payment_option" name="reg[payment_option]" value="Credit Card" /> 
                                        <br>
                                        <label for="payment_option">Credit Card</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="payment_option" name="reg[payment_option]" value="Paypal" /> 
                                        <br>
                                        <label for="payment_option">Paypal</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="payment_option" name="reg[payment_option]" value="Western Union" /> 
                                        <br>
                                        <label for="payment_option">Western Union</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="payment_option" name="reg[payment_option]" value="Except from Payment" /> 
                                        <br>
                                        <label for="payment_option">Except from Payment</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="padding-right: 10%;">

                    
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Your Picture</label>
                        <div class="col-sm-8">
                            <div class="profile-img-section"></div>
                        </div>
                         <img id="previewimage" onclick="$('#uploadFile').click();" src="<?php echo base_url(); ?>images/default.jpg" style="cursor: pointer;height: 250px;margin-left:7%; position: relative;z-index: 10;"/>
                            <input type="file" id="uploadFile" name="profile_img" style="position: absolute; margin: 0px auto; visibility: hidden;"  />
                           
                        <!--<input type="file" class="form-control" name="profile_img" id="img" >-->
                    </div>
                    <div class="reg-address">
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Address Line1<span>*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="reg[address_line1]" id="address_line1" class="form-control" />
                                <label for="address_line1" generated="true" class="error text-red" style="display:none;"></label>
                                <!--<div id='error_emailid'></div>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Address Line2<span>*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="reg[address_line2]" id="address_line2" class="form-control" />
                                <label for="address_line2" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Town/City<span>*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="reg[city]" id="city" class="form-control" />
                                <label for="city" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">State<span>*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="reg[state]" id="state" class="form-control" />
                                <label for="state" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Postal Code<span>*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="reg[postal_code]" id="postal_code" class="form-control" />
                                <label for="postal_code" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Country<span>*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="reg[country]" id="country" class="form-control" />
                                <label for="country" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Nationality<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[nationality]" id="nationality" class="form-control" />
                            <label for="nationality" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Passport Number<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[passport_no]" id="passport_no" class="form-control" />
                            <label for="passport_no" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-10 control-label">How Many Umrah Pilgrims Do You Take Annually?</label>
                        <div class="col-sm-8">
                            <input type="text" name="umrah_pilgrims_annually" id="umrah_pilgrims_annually" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-10 control-label">Would You Like To Subscribe to the Hajj People Membership Program?</label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="hajj_people_membership" name="reg[hajj_people_membership]" value="1" /> 
                                        <br>
                                        <label for="hajj_people_membership">Yes</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="hajj_people_membership" name="reg[hajj_people_membership]" value="2" /> 
                                        <br>
                                        <label for="hajj_people_membership">No</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" style="font-size:15px; margin-bottom: 3%;">
                            Social Media ID
                        </label>
                        <div class="col-sm-10" style="margin-bottom: 2%;">
                            <div class="col-sm-2 col-xs-2">
                                <img src="<?= base_url("images/f1.png"); ?>" style="width: 40px; height: 40px;">
                            </div>
                            <div class="col-sm-7 col-xs-10">
                                <input type="text" name="reg[social_id_fb]" id="social_id_fb" class="form-control" />
                                <span class="text-sm">facebook.com/</span>
                            </div>
                        </div>
                        <div class="col-sm-10" style="margin-bottom: 2%;">
                            <div class="col-sm-2 col-xs-2">
                                <img src="<?= base_url("images/t1.png"); ?>" style="width: 40px; height: 40px;">
                            </div>
                            <div class="col-sm-7 col-xs-10">
                                <input type="text" name="reg[social_id_twitter]" id="social_id_twitter" class="form-control" />
                                <span class="text-sm">twitter.com/</span>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-2 col-xs-2">
                                <img src="<?= base_url("images/i1.png"); ?>" style="width: 40px; height: 40px;">
                            </div>
                            <div class="col-sm-7 col-xs-10">
                                <input type="text" name="reg[social_id_instagram]" id="social_id_instagram" class="form-control" />
                                <span class="text-sm">instagram.com/</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 text-right">
                    <div class="alert-error" id="error_server" style="display: none;">

                    </div>
                    <div id="loaderspan" style="display:none;">
                        <img src="<?= base_url('images/loader1.gif'); ?>">
                    </div>
                    <input type="submit" id="sbmt" class="btn btn-warning submitBtn" value="Submit" />
                </div>
            </form>
        </div> <!--row-->
    </div> <!--panel body-->
</section>

<script type="text/javascript">
    $('#dob').mask('99/99/9999');

    function formvalidation() {
        var validator = $("#regForm").validate({
            rules: {
                "reg[participate_in]": {
                    required: true
                },
                "reg[participate_as]": {
                    required: true
                },
                "reg[company_name]": {
                    required: true
                },
                "reg[contact_name]": {
                    required: true
                },
                "reg[job_title]": {
                    required: true
                },
                "reg[office_telephone]": {
                    required: true,
                    digits: true
                },
                "reg[mobile_no]": {
                    required: true,
                    digits: true
                },
                "reg[email]": {
                    required: true,
                    email: true
                },
                "reg[gender]": {
                    required: true
                },
                "dob": {
                    required: true
                },
                "reg[company_business_nature]": {
                    required: true
                },
                "reg[passport_no]": {
                    required: true
                },
                "reg[nationality]": {
                    required: true
                },
                "reg[country]": {
                    required: true
                },
                "reg[postal_code]": {
                    required: true,
                    digits: true
                },
                "reg[state]": {
                    required: true
                },
                "reg[city]": {
                    required: true
                },
                "reg[address_line2]": {
                    required: true
                },
                "reg[address_line1]": {
                    required: true
                }
            },
            messages: {
                "reg[participate_in]": {
                    required: "This field is required."
                },
                "reg[participate_as]": {
                    required: "This field is required."
                },
                "reg[company_name]": {
                    required: "This field is required.",
                },
                "reg[contact_name]": {
                    required: "This field is required."
                },
                "reg[job_title]": {
                    required: "This field is required."
                },
                "reg[office_telephone]": {
                    required: "This field is required."
                },
                "reg[mobile_no]": {
                    required: "This field is required."
                },
                "reg[email]": {
                    required: "This field is required.",
                    email: "Enter an valid email."
                },
                "reg[gender]": {
                    required: "This field is required."
                },
                "dob": {
                    required: "This field is required."
                },
                "reg[company_business_nature]": {
                    required: "This field is required."
                },
                "reg[passport_no]": {
                    required: "This field is required."
                },
                "reg[nationality]": {
                    required: "This field is required."
                },
                "reg[country]": {
                    required: "This field is required."
                },
                "reg[postal_code]": {
                    required: "This field is required."
                },
                "reg[state]": {
                    required: "This field is required."
                },
                "reg[city]": {
                    required: "This field is required."
                },
                "reg[address_line2]": {
                    required: "This field is required."
                },
                "reg[address_line1]": {
                    required: "This field is required."
                }
            }
        });
        var x = validator.form();
        if (x) {
            $("#sbmt").prop('disabled', true);
            $('#loaderspan').show();
            return true;
        } else {
            $("#sbmt").prop('disabled', false);
            return false;
        }
    }
    function calbackFun(result) {
        $('#loaderspan').hide();
        var data = JSON.parse(result);
        if (data.hasError == true) {
            if (data.servermessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.servermessage + "</h4>");
                return false;
            } else if (data.methodmessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.methodmessage + "</h4>");
                return false;
            }
        } else {
            $("#sbmt").prop('disabled', false);
            $('#success_submit').show();
            setTimeout(function () {
                $('#success_submit').hide();
                window.location.href = "" + data.redirecturl + "";
            }, 10000);
            return false;
        }
    }
    $(document).ready(function(){
        
         $("#uploadFile").change(function () {
        readURL(this);
    });
    });
   
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#previewimage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

<?php
include SITE_ROOT . 'views/regFooter.php';
?>

