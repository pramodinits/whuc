<?php
include SITE_ROOT . 'views/header.php';
?>
<style>
    .col_one_sixth {
        margin-right: 2% !important;
    }
    .col_full {
        margin-left: 2%;
        margin-bottom: 10px !important;
    }
    .col_one_fourth {
        text-align: center;
    }
    .col_one_sixth img{
        width: 100%;
margin-top: 5%;
    }
    .footerlogo-text {
        font-size: 1em;
        color: darkgray;
    }
    .mt-4 {
        margin-top: 4%;
    }
</style>
<!-- Content ============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
         <p class="style4"><strong>Contact Us</strong></p>
          <tr>
            <td colspan="2"><p class="style4">Hajj People Ltd<br />
                Vitesse House<br />
                6 Estate Way<br />
                Leyton<br />
                London<br />
                E10 7JW<br />
                <br />
                Tel: +44 (0) 870 042 0000<br />
                email: <a href="mailto: info@whuc.org">info@whuc.org</a></p>
                <p class="style4"><strong>Already a Registered Delegate?<br />
                <br />
                </strong>We'd love to hear back from you.<br />
                <strong>Contact:</strong> <a href="mailto: feedback@hajjpeople.com">Feedback@hajjpeople.com</a></p>
                <p class="style4"><strong>Press Office</strong><br />
                email: <a href="mailto: press@hajjpeople.com">press@hajjpeople.com</a></p>
            </td>
            <td width="1">&nbsp;</td>
           </tr>


            
            <div class="clearfix"></div>
            
            <div class="clear"></div>
        </div>
    </div>			
</section><!-- #content end -->
<?php include SITE_ROOT . 'views/footer.php' ?>