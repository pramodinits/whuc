<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="SemiColonWeb" />

        <!-- Stylesheets
        ============================================= -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/swiper.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/dark.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/font-icons.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/animate.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/switcher.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/magnific-popup.css'); ?>" type="text/css" />

        <link rel="stylesheet" href="<?php echo base_url('css/responsive.css'); ?>" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lt IE 9]>
                <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

        <!-- Document Title
        ============================================= -->
        <title>WHUC - Registration</title>
        <script type="text/javascript" src="<?php echo base_url('js/jquery.js'); ?>"></script>
        <script src="<?= base_url('js/jquery.mask.js'); ?>"></script>
    </head>

    <body class="stretched">

        <!-- Document Wrapper
        ============================================= -->
        <div id="wrapper" class="clearfix">

            <!-- Header
            ============================================= -->
            <header id="header" >

                <div id="header-wrap" style="height: 130px;">

                    <div class="container clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <div id="logo">
                                    <?php if($logo){?>
                                    <a href="<?php echo base_url();?>" class="standard-logo" data-dark-logo="<?php echo base_url().$logo; ?>">
                                        <img src="<?php echo base_url().$logo; ?>" style="margin-top: 8%;" alt="<?php echo $title;?>">
                                    </a>
                                    <?php }else{ ?>
                                     <a href="<?php echo base_url();?>" class="standard-logo" data-dark-logo="<?php echo base_url('images/reg-logo.png'); ?>">
                                        <img src="<?php echo base_url('images/reg-logo.png'); ?>" style="margin-top: 8%;" alt="<?php echo $title;?>">
                                    </a>
                                    <span><?php echo $title;?></spam>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="uline" id="success_submit">
                                    Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 regheader-right">
                                <div class="col-md-6 col-xs-6 text-right" style="padding: 0 !important;">
                                    <span style="font-size: 14px; color: #969696;">
                                        Connect now
                                    </span>
                                </div>
                                <div class="col-md-6 col-xs-5 text-right" style="padding: 0 !important;">
                                <div class="nobottommargin col_last tright" style="font-size: .9em;">
                                    <div class="fright clearfix" style="margin-top: -5%;">
                                        <a href="https://www.facebook.com/hajjpeople09" target="_blank" class="social-icon si-small si-borderless">
                                            <img src="<?= base_url('images/f1.png'); ?>">
                                        </a>
                                        <a href="https://www.twitter.com/hajjpeople_whuc" target="_blank" class="social-icon si-small si-borderless">
                                            <img src="<?= base_url('images/t1.png'); ?>">
                                        </a>

                                        <a href="https://www.instagram.com/official.whuc" target="_blank" class="social-icon si-small si-borderless">
                                            <img src="<?= base_url('images/i1.png'); ?>">
                                        </a>
                                    </div>

                                </div>
                                </div>
                                <br>
                                <span style="font-size: 25px; color: #5d1a6e;">
                                    Registration Form
                                </span>
                            </div>
                        </div>


                        <!-- Logo
                        ============================================= -->
                        <!-- #logo end -->



                    </div>
                </div>

            </header><!-- #header end -->
