<!-- Footer
                ============================================= -->
<footer id="footer" class="dark">

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">
            <div class="col-md-12">
                <div class="col-md-6 col-xs-12">
                    <i class="icon-call" aria-hidden="true" style="font-size: 1.5em;position: relative;right: -73%;"></i>
                    <span style="position: absolute; top: 10%; right: 0;">&nbsp;+44 (0)208 252 43 63</span>
                </div>
                <div class="col-md-6 col-xs-12 text-left">
                    <i class="icon-envelope" aria-hidden="true" style="font-size: 1.5em;"></i>
                    <span style="position: absolute; top: 10%;">&nbsp;info@hajjpeople.com</span>
                </div>
            </div>
            
            
        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<!--<script type="text/javascript" src="<?//= base_url('js/jquery.js');?>"></script>-->
<script type="text/javascript" src="<?= base_url('js/plugins.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('js/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('js/ajaxupload.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="<?= base_url('js/functions.js'); ?>"></script>
<!-- Number Slider JavaScripts
============================================= -->


</body>
</html>