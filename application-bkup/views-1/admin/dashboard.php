<?php include SITE_ROOT . 'views/adminHeader.php'; ?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Welcome</h2>
    </header>

    <!-- start: page -->

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="row">
            <div class="col-md-6 col-xl-12">
                <section class="panel">
                    <div class="panel-body bg-primary">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon">
                                    <i class="fa fa-life-ring"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Hajj People Islamic Tourism</h4>
                                    <div class="info">
                                        <strong class="amount"><?= $userCountFlag1; ?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-uppercase" href="<?= base_url('admin/userListing/1') ?>">(view all)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6 col-xl-12">
                <section class="panel">
                    <div class="panel-body bg-secondary">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon">
                                    <i class="fa fa-users"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">WHUC Forum</h4>
                                    <div class="info">
                                        <strong class="amount"><?= $userCountFlag2; ?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-uppercase" href="<?= base_url('admin/userListing/2') ?>">(view all)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6 col-xl-12">
                <section class="panel">
                    <div class="panel-body bg-tertiary">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon">
                                    <i class="fa  fa-plus-circle"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">DIXIE Queen</h4>
                                    <div class="info">
                                        <strong class="amount"><?= $userCountFlag3; ?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-uppercase" href="<?= base_url('admin/userListing/3') ?>">(view all)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6 col-xl-12">
                <section class="panel">
                    <div class="panel-body bg-quartenary">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Hajj People Awards</h4>
                                    <div class="info">
                                        <strong class="amount"><?= $userCountFlag4; ?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-uppercase" href="<?= base_url('admin/userListing/4') ?>">(view all)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- end: page -->
</section>
<?php include SITE_ROOT . 'views/adminFooter.php' ?>
