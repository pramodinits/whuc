<?php
include SITE_ROOT . 'views/adminHeader.php';
?>
<section role="main" class="content-body">

    <header class="page-header">
        <h2 class="panel-title">Users</h2>
    </header>

    <div class="panel-body">
        <div id="msg" class="btn btn-block btn-xs" style="display: none;"></div>
        <h3 class="innerHdng">User Listing</h3>
        <table class="table table-hover table-bordered table-striped dt-responsive nowrap" id="userListing" style="width:90%;">
            <thead class="">
                <tr>
                    <th class="desktop">Name Of Company</th>
                    <th class="desktop">Contact Full Name</th>
                    <th class="desktop">Job Title</th>
                    <th class="desktop">Mobile</th>
                    <th class="desktop">Email</th>
                    <th class="desktop">Registration Type</th>
                    <th class="desktop">Action</th>
                    <!--<th class="desktop">Edit</th>-->
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($userList)) {
                    foreach ($userList as $k => $v) {
                        ?>
                        <tr>
                            <td><?= htmlentities($v['company_name']); ?></td>
                            <td><?= htmlentities($v['contact_name']); ?></td>
                            <td><?php echo htmlentities($v['job_title']); ?></td>
                            <td><?php echo htmlentities($v['mobile_no']); ?></td>
                            <td><?= $v['email']; ?></td>
                            <td><?php
                                if ($v['flag'] == 1) {
                                    echo "Hajj People Islamic Tourism Expo";
                                } else if ($v['flag'] == 2) {
                                    echo "WHUC Forum";
                                } else if ($v['flag'] == 3) {
                                    echo "DIXIE Queen";
                                } else if ($v['flag'] == 4) {
                                    echo "Hajj People Awards";
                                }
                                ?></td>
                            <td>
                                <a href="<?= base_url("admin/viewUser/?reg_id=" . ($v['reg_id'])); ?>">
                                    <i class="fa fa-eye text-success" rel="tooltip" data-original-title="View User"></i>
                                </a>
                            </td>
<!--                            <td>
                                <a href="<?= base_url("admin/editUser/?reg_id=" . ($v['reg_id'])); ?>">
                                    <i class="fa fa-pencil text-success" rel="tooltip" data-original-title="Edit User"></i>
                                </a> 
                            </td>-->
                        </tr>

                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="8">No record found !!!</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

</section><!-- #page-title end -->
<script>
    
    $(document).ready(function () {
        $('#userListing').DataTable({
            "pageLength": 10,
            "searching": true, // Search Box will Be Disabled
            
            "ordering": true, // Ordering (Sorting on Each Column)will Be Disabled

            "info": true, // Will show "1 to n of n entries" Text at bottom

            "lengthChange": false,
            "scrollY":        "200px",
             "scrollX": true,
        "scrollCollapse": true,
        });
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
    });
</script>
<!--datatables-->
    </script>
<?php
include SITE_ROOT . 'views/adminFooter.php';
?>
