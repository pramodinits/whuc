<?php
include SITE_ROOT . 'views/adminHeader.php';
?>
<style>
    table {
        width: 100% !important;
        table-layout: fixed;
    }
    table td {
        word-wrap: break-word;
        white-space: inherit; 
        white-space: -moz-pre-wrap; 
        white-space: -pre-wrap;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: inherit !important;
    }
    div.dataTables_wrapper div.dataTables_filter {
        margin-right: 12%;
    }
</style>
<section role="main" class="content-body">

    <header class="page-header">
        <h2 class="panel-title">Users</h2>
    </header>
    <div class="panel-body">
        <?php if ($this->session->flashdata('message')) { ?>
            <div class="alert alert-success">      
                <strong>
                <?php echo $this->session->flashdata('message') ?>
                </strong>
            </div>
        <?php } ?>
        <div id="msg" class="btn btn-block btn-xs" style="display: none;"></div>
        <h3 class="innerHdng col-md-6" style="padding:0;">User Listing Unverified</h3>
        <form method="post" action="<?= base_url('admin/deleteAllChecked'); ?>" onSubmit="return delete_confirm();">
            <?php if (!empty($userList)) { ?>
            <div style="margin: 2% 0;" class="text-right col-md-6">
                <h5>
                    <input type="checkbox" id="checkAll">&nbsp;<strong>Select All</strong>
                    <input type="hidden" name="redirecturl" value="userListingUnverified">
                    <button class="btn btn-primary" id="sbmt" style="padding: 3px 8px; margin-left: 5px;">Delete</button>
                </h5>
            </div>
            <?php } ?>
            <table class="table table-hover table-bordered table-striped dt-responsive nowrap" id="userListing" style="width:90%;">
                <thead class="">
                    <tr>
                        <th></th>
                        <th class="desktop">Name Of Company</th>
                        <th class="desktop">Contact Full Name</th>
                        <th class="desktop">Job Title</th>
                        <th class="desktop">Mobile</th>
                        <th class="desktop">Email</th>
                        <th class="desktop">Registration Type</th>
                        <th class="desktop">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($userList)) {
                        foreach ($userList as $k => $v) {
                            ?>
                            <tr>
                                <td><input type="checkbox" class="checkusers" id="checkUsers" name="deleteUsers[]" value="<?php echo $v["reg_id"]; ?>"></td>
                                <td><?= htmlentities($v['company_name']); ?></td>
                                <td><?= htmlentities($v['contact_name']); ?></td>
                                <td><?php echo htmlentities($v['job_title']); ?></td>
                                <td><?php echo htmlentities($v['mobile_no']); ?></td>
                                <td><?= $v['email']; ?></td>
                                <td><?php
                                    if ($v['flag'] == 1) {
                                        echo "Hajj People Islamic Tourism Expo";
                                    } else if ($v['flag'] == 2) {
                                        echo "WHUC Forum";
                                    } else if ($v['flag'] == 3) {
                                        echo "DIXIE Queen";
                                    } else if ($v['flag'] == 4) {
                                        echo "Hajj People Awards";
                                    }
                                    ?></td>
                                <td>
                                    <a href="<?= base_url("admin/viewUser/?reg_id=" . ($v['reg_id'])); ?>">
                                        <i class="fa fa-eye text-success" rel="tooltip" data-original-title="View User"></i>
                                    </a> &nbsp;
                                    <a href="<?= base_url("admin/userEdit/?reg_id=" . ($v['reg_id']) . "&flag=" . ($v['flag'])); ?>">
                                        <i class="fa fa-pencil text-success" rel="tooltip" data-original-title="Edit User"></i>
                                    </a> &nbsp;
                                    <a href='javascript:void(0)' onclick="deleteUser(<?= $v['reg_id']; ?>);">
                                        <i class="fa fa-trash-o text-danger" rel="tooltip" data-original-title="Delete User"></i>
                                    </a>
                                </td>
                            </tr>

                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="7" class="text-center">No record found !!!</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </form>
    </div>

</section><!-- #page-title end -->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css" />

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>

<script>
                                $(window).bind("load", function () {
                                    window.setTimeout(function () {
                                        $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                            $(this).remove();
                                        });
                                    }, 4000);
                                });

                                //to check all
                                $(document).ready(function () {
                                    $('#checkAll').on('click', function () {
                                        if (this.checked) {
                                            $('.checkusers').each(function () {
                                                this.checked = true;
                                            });
                                        } else {
                                            $('.checkusers').each(function () {
                                                this.checked = false;
                                            });
                                        }
                                    });

                                    $('.checkusers').on('click', function () {
                                        if ($('.checkusers:checked').length === $('.checkusers').length) {
                                            $('#checkAll').prop('checked', true);
                                        } else {
                                            $('#checkAll').prop('checked', false);
                                        }
                                    });
                                });
                                //delete confirm 
                                function delete_confirm() {
                                    if ($('.checkusers:checked').length > 0) {
                                        var result = confirm("Are you sure to delete selected users?");
                                        if (result) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    } else {
                                        alert('Select at least 1 record to delete.');
                                        return false;
                                    }
                                }

                                function deleteUser(reg_id) {
                                    var url = "<?= base_url('admin/deleteUser/'); ?>";
                                    var conf = confirm("Are You sure to delete this User?");
                                    if (!conf) {
                                        return false;
                                    }
                                    $.post(url, {"reg_id": reg_id}, function (res) {
                                        alert("Delete User successfully.");
                                        location.reload();
                                    });
                                }

                                $(document).ready(function () {
                                    $('#userListingBC').DataTable({
                                        "pageLength": 10,
                                        "searching": true, // Search Box will Be Disabled

                                        "ordering": true, // Ordering (Sorting on Each Column)will Be Disabled

                                        "info": true, // Will show "1 to n of n entries" Text at bottom

                                        "lengthChange": false,
                                        "scrollY": "600px",
                                        "scrollX": true,
                                        "scrollCollapse": true,
                                    });
                                });
                                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                                    $($.fn.dataTable.tables(true)).DataTable()
                                            .columns.adjust();
                                });
                                $(document).ready(function () {
                                    $('#userListing').DataTable({
                                        "pageLength": 10,
                                        "searching": true, // Search Box will Be Disabled

                                        "ordering": true, // Ordering (Sorting on Each Column)will Be Disabled

                                        "info": true, // Will show "1 to n of n entries" Text at bottom

                                        "lengthChange": false,
                                        "scrollY": "600px",
                                        "scrollX": true,
                                        "scrollCollapse": true,
                                        dom: 'Bfrtip',
                                        buttons: [
                                            'copyHtml5',
                                            'excelHtml5',
                                            'csvHtml5',
                                            'pdfHtml5'
                                        ]
                                    });
                                });

</script>

<?php
include SITE_ROOT . 'views/adminFooter.php';
?>
