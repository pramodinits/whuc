<section role="main" class="content-body">
    <header class="page-header">
        <h2 class="panel-title">Users</h2>
    </header>
    <!--form section-->
    <section class="panel">
        <header class="panel-heading row">
            <div class="col-md-6"><h2 class="panel-title">Edit User</h2></div>
            <div class="col-md-6 text-right">
                <a href="<?= $redirecturl; ?>">
                            <button class="btn btn-primary" id="sbmt">
                                Back
                            </button>
                        </a>
            </div>
        </header>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" name="regForm" id="regForm" action="<?= base_url('admin/editUser/'); ?>" method="post" enctype="multipart/form-data" onsubmit="return AsyncUpload.submitForm(this, formvalidation, calbackFun);">
                <input type="hidden" name="reg_id" value="<?php echo $userDetail['reg_id']; ?>" id="reg_id" />
                <input type="hidden" name="reg[flag]" value="<?php echo $userDetail['flag']; ?>" id="flag" />
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">
                        Participate In<span>*</span>
                    </label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="London" name="reg[participate_in]" value="London" <?= $userDetail['participate_in'] == "London" ? 'checked' : "" ?> /> 
                            <label for="London">London</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Paris" name="reg[participate_in]" value="Paris" <?= $userDetail['participate_in'] == "Paris" ? 'checked' : "" ?> /> 
                            <label for="Paris">Paris</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Cologne" name="reg[participate_in]" value="Cologne" <?= $userDetail['participate_in'] == "Cologne" ? 'checked' : "" ?> /> 
                            <label for="Cologne">Cologne</label>
                        </div>
                        <label for="participate_in" generated="true" class="error" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">
                        Participate As<span>*</span>
                    </label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Visitor" name="reg[participate_as]" value="Visitor" <?= $userDetail['participate_as'] == "Visitor" ? 'checked' : "" ?> /> 
                            <label for="Visitor">Visitor</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Exhibitor" name="reg[participate_as]" value="Exhibitor" <?= $userDetail['participate_as'] == "Exhibitor" ? 'checked' : "" ?> /> 
                            <label for="Exhibitor">Exhibitor</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Sponsor" name="reg[participate_as]" value="Sponsor" <?= $userDetail['participate_as'] == "Sponsor" ? 'checked' : "" ?> /> 
                            <label for="Sponsor">Sponsor</label>
                        </div>
                        <label for="participate_as" generated="true" class="error" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Name Of Company<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[company_name]" value="<?php echo $userDetail['company_name'] ?>" id="company_name" class="form-control" />
                        <label for="company_name" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Contact Full Name<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[contact_name]" value="<?php echo $userDetail['contact_name'] ?>" id="contact_name" class="form-control" />
                        <label for="contact_name" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Job Title<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[job_title]" value="<?php echo $userDetail['job_title'] ?>" id="job_title" class="form-control" />
                        <label for="job_title" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Office Tel</label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[office_telephone]" value="<?php echo $userDetail['office_telephone'] ?>" id="office_telephone" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Mobile / Cellphone<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[mobile_no]" value="<?php echo $userDetail['mobile_no'] ?>" id="mobile_no" class="form-control" />
                        <label for="mobile_no" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Email<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[email]" value="<?php echo $userDetail['email'] ?>" id="email" class="form-control" />
                        <label for="email" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Gender<span>*</span></label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Male" name="reg[gender]" value="1" <?= $userDetail['gender'] == "1" ? 'checked' : "" ?> /> 
                            <label for="Male">Male</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Female" name="reg[gender]" value="2" <?= $userDetail['gender'] == "2" ? 'checked' : "" ?> /> 
                            <label for="Female">Female</label>
                        </div>
                        <label for="gender" generated="true" class="error" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Address Line1<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[address_line1]" value="<?php echo $userDetail['address_line1'] ?>" id="address_line1" class="form-control" />
                        <label for="address_line1" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Address Line2</label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[address_line2]" value="<?php echo $userDetail['address_line2'] ?>" id="address_line2" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Town/City<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[city]" value="<?php echo $userDetail['city'] ?>" id="city" class="form-control" />
                        <label for="city" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">State/Provinces</label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[state]" value="<?php echo $userDetail['state'] ?>" id="state" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Postal Code</label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[postal_code]" value="<?php echo $userDetail['postal_code'] ?>" id="postal_code" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Country<span>*</span></label>
                    <div class="col-sm-6">
                        <select name="reg[country]" id="countryreg" class="form-control">
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Åland Islands">Åland Islands</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antarctica">Antarctica</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">Bouvet Island</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote D'ivoire">Cote D'ivoire</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Territories">French Southern Territories</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guernsey">Guernsey</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-bissau">Guinea-bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                    <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Isle of Man">Isle of Man</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jersey">Jersey</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                    <option value="Korea, Republic of">Korea, Republic of</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macao">Macao</option>
                                    <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                    <option value="Moldova, Republic of">Moldova, Republic of</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montenegro">Montenegro</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russian Federation">Russian Federation</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Helena">Saint Helena</option>
                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                    <option value="Saint Lucia">Saint Lucia</option>
                                    <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                    <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia">Serbia</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                    <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Timor-leste">Timor-leste</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States">United States</option>
                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Viet Nam">Viet Nam</option>
                                    <option value="Virgin Islands, British">Virgin Islands, British</option>
                                    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                    <option value="Wallis and Futuna">Wallis and Futuna</option>
                                    <option value="Western Sahara">Western Sahara</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                        <label for="country" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Nationality<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[nationality]" value="<?php echo $userDetail['nationality'] ?>" id="nationality" class="form-control" />
                        <label for="nationality" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Nature Of Company Business<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[company_business_nature]" value="<?php echo $userDetail['company_business_nature'] ?>" id="company_business_nature" class="form-control" />
                        <label for="company_business_nature" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">How many Hajj Pilgrims Do You Take Annually?<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="hajj_pilgrims_annually" value="<?php echo $userDetail['hajj_pilgrims_annually'] ?>" id="hajj_pilgrims_annually" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">How Many Umrah Pilgrims Do You Take Annually?<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="umrah_pilgrims_annually" value="<?php echo $userDetail['umrah_pilgrims_annually'] ?>" id="umrah_pilgrims_annually" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Would You Like to become a member of the World Hajj Umrah Organisers Association?<span>*</span></label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_umrah_organisers_yes" name="reg[hajj_umrah_organisers_assocciation_membership]" value="1" 
                                   <?= $userDetail['hajj_umrah_organisers_assocciation_membership'] == "1" ? 'checked' : "" ?> /> 
                            <label for="hajj_umrah_organisers_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_umrah_organisers_no" name="reg[hajj_umrah_organisers_assocciation_membership]" value="2" 
                                   <?= $userDetail['hajj_umrah_organisers_assocciation_membership'] == "2" ? 'checked' : "" ?> /> 
                            <label for="hajj_umrah_organisers_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Would You Like to become a member of the British Hajj and Umrah Council?<span>*</span></label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="british_hajj_umrah_yes" name="reg[british_hajj_umrah_council_membership]" value="1" 
                                   <?= $userDetail['british_hajj_umrah_council_membership'] == "1" ? 'checked' : "" ?> /> 
                            <label for="british_hajj_umrah_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="british_hajj_umrah_no" name="reg[british_hajj_umrah_council_membership]" value="2" 
                                   <?= $userDetail['british_hajj_umrah_council_membership'] == "2" ? 'checked' : "" ?> /> 
                            <label for="british_hajj_umrah_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        Would You Like to become a member of the National Pilgrimage Organisers Association?</label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="national_pilgrimage_yes" name="reg[national_pilgrimage_organisers_association]" value="1" 
                                   <?= $userDetail['national_pilgrimage_organisers_association'] == "1" ? 'checked' : "" ?> /> 
                            <label for="national_pilgrimage_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="national_pilgrimage_no" name="reg[national_pilgrimage_organisers_association]" value="2" 
                                   <?= $userDetail['national_pilgrimage_organisers_association'] == "2" ? 'checked' : "" ?> /> 
                            <label for="national_pilgrimage_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        Would You Like To Subscribe to the Hajj People Membership Program?</label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_people_membership_yes" name="reg[hajj_people_membership]" value="1" 
                                   <?= $userDetail['hajj_people_membership'] == "1" ? 'checked' : "" ?> /> 
                            <label for="hajj_people_membership_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_people_membership_no" name="reg[hajj_people_membership]" value="2" 
                                   <?= $userDetail['hajj_people_membership'] == "2" ? 'checked' : "" ?> /> 
                            <label for="hajj_people_membership_no">No</label>
                        </div>
                    </div>
                </div>
                <h3>Social Media ID</h3>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        <img src="<?= base_url("images/f1.png"); ?>" style="width: 40px; height: 40px;margin-top: -5%;">
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[social_id_fb]" value="<?php echo $userDetail['social_id_fb'] ?>" id="social_id_fb" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        <img src="<?= base_url("images/t1.png"); ?>" style="width: 40px; height: 40px;margin-top: -5%;">
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[social_id_twitter]" value="<?php echo $userDetail['social_id_twitter'] ?>" id="social_id_twitter" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        <img src="<?= base_url("images/i1.png"); ?>" style="width: 40px; height: 40px;margin-top: -5%;">
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[social_id_instagram]" value="<?php echo $userDetail['social_id_instagram'] ?>" id="social_id_instagram" class="form-control" />
                    </div>
                </div>
                <?php if ($userDetail['email_verify'] != 1) { ?>
                 <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                       &nbsp;
                    </label>
                    <div class="col-sm-6">
                        <input type="checkbox" name="reg[email_verify]" value="1" id="email_verify" class="form-control" /> 
                        <label for="email_verify" class="text-bold" style="font-size: medium;">Verify User</label>
                    </div>
                </div>
                <?php } ?>
                <div class="alert-error" id="error_server" style="padding: 0 0 5px 0; display: none; text-align: center;">

                </div>
                <div class="alert-error" id="error_server" style="padding: 0 0 5px 0; display: none; text-align: center;">

                </div>
                <div id="loaderspan" style="display:none;">
                    <img src="<?= base_url('images/loader1.gif'); ?>">
                </div>
                <div class="uline" id="success_submit">
                    Your inquiry was updated successfully.
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="sbmt"></label>
                    <div class="col-md-9">
                        <button class="btn btn-primary" id="sbmt">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>