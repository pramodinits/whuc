<?php
include SITE_ROOT . 'views/regHeader.php';
?>
<style>
    label[for="reg[participate_in]"],
    label[for="reg[participate_as]"],
    label[for="reg[gender]"]{
        position: absolute;
        top: 75%;
        text-align: left;
        color: #E42C3E !important;
    }

</style>
<div class="clearfix"></div>
<section role="main" class="content-body">
    <!--    <header class="page-header">
            <h2 class="panel-title">SSTA Users</h2>
        </header>-->
    <div class="panel-body">
        <div id="msg" class="btn btn-block btn-xs" style="display: none;"></div>
        <div class="row">
            <form class="form-horizontal form-bordered" name="regForm" id="regForm"  style="float: left;" action="<?= base_url('user/insertregUser/'); ?>" method="post" enctype="multipart/form-data" onsubmit="return AsyncUpload.submitForm(this, formvalidation, calbackFun);">
                <input type="hidden" name="id_user" value="<?php if (!empty($res['id_user'])) echo @$res['id_user'] ?>" id="id_user" />
                <input type="hidden" name="reg[flag]" value="3" id="flag" />
                <div class="col-md-6" style="padding-left: 10%;">
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Participate In<span>*</span></label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="London" checked="checked" name="reg[participate_in]" value="London" /> 
                                        <br>
                                        <label for="London">London</label>
                                    </li>
                                    <!--                                    <li>
                                                                            <input type="radio" id="Paris" name="reg[participate_in]" value="Paris" /> 
                                                                            <br>
                                                                            <label for="Paris">Paris</label>
                                                                        </li>
                                                                        <li>
                                                                            <input type="radio" id="Cologne" name="reg[participate_in]" value="Cologne" /> 
                                                                            <br>
                                                                            <label for="Cologne">Cologne</label>
                                                                        </li>-->
                                </ul>
                            </div>
                            <label for="participate_in" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Participate As<span>*</span></label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="Visitor" name="reg[participate_as]" value="Visitor" /> 
                                        <br>
                                        <label for="Visitor">Visitor</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="Exhibitor" name="reg[participate_as]" value="Exhibitor" /> 
                                        <br>
                                        <label for="Exhibitor">Exhibitor</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="Sponsor" name="reg[participate_as]" value="Sponsor" /> 
                                        <br>
                                        <label for="Sponsor">Sponsor</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Name Of Company<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[company_name]" value="" id="company_name" class="form-control" />
                            <label for="company_name" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Contact Full Name<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[contact_name]" value="" id="contact_name" class="form-control" />
                            <label for="contact_name" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Job Title<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[job_title]" value="" id="job_title" class="form-control" />
                            <label for="job_title" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Office Tel</label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[office_telephone]" value="" id="office_telephone" class="form-control" />
                            <!--<label for="office_telephone" generated="true" class="error text-red" style="display:none;"></label>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Mobile / Cellphone<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[mobile_no]" value="" id="mobile_no" class="form-control" />
                            <label for="mobile_no" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Email<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[email]" value="" id="email" class="form-control" />
                            <label for="email" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Gender<span>*</span></label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="Male" name="reg[gender]" value="1" /> 
                                        <br>
                                        <label for="Male">Male</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="Female" name="reg[gender]" value="2" /> 
                                        <br>
                                        <label for="Female">Female</label>
                                    </li>
                                </ul>
                            </div>
                            <label for="gender" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-10 control-label">How many Hajj Pilgrims Do You Take Annually?</label>
                        <div class="col-sm-8">
                            <input type="text" name="hajj_pilgrims_annually" value="" id="hajj_pilgrims_annually" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-10 control-label">How Many Umrah Pilgrims Do You Take Annually?</label>
                        <div class="col-sm-8">
                            <input type="text" name="umrah_pilgrims_annually" id="umrah_pilgrims_annually" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-9 control-label">Would You Like to become a member of the World Hajj Umrah Organisers Association?</label>
                        <div class="col-sm-9">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="hajj_umrah_organisers_yes" name="reg[hajj_umrah_organisers_assocciation_membership]" value="1" /> 
                                        <br>
                                        <label for="hajj_umrah_organisers_yes">Yes</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="hajj_umrah_organisers_no" name="reg[hajj_umrah_organisers_assocciation_membership]" value="2" /> 
                                        <br>
                                        <label for="hajj_umrah_organisers_no">No</label>
                                    </li>
                                </ul>
                            </div>
                            <label for="hajj_umrah_organisers" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-10 control-label">Would You Like To Subscribe to the Hajj People Membership Program?</label>
                        <div class="col-sm-8">
                            <div class="radio-custom radio-primary">
                                <ul>
                                    <li>
                                        <input type="radio" id="hajj_people_membership_yes" name="reg[hajj_people_membership]" value="1" /> 
                                        <br>
                                        <label for="hajj_people_membership_yes">Yes</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="hajj_people_membership_no" name="reg[hajj_people_membership]" value="2" /> 
                                        <br>
                                        <label for="hajj_people_membership_no">No</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="padding-right: 7%;">
                    <div class="reg-address">
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Address Line1<span>*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="reg[address_line1]" id="address_line1" class="form-control" />
                                <label for="address_line1" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Address Line2</label>
                            <div class="col-sm-8">
                                <input type="text" name="reg[address_line2]" id="address_line2" class="form-control" />
                                <label for="address_line2" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Town/City<span>*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="reg[city]" id="city" class="form-control" />
                                <label for="city" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">State/Provinces</label>
                            <div class="col-sm-8">
                                <input type="text" name="reg[state]" id="state" class="form-control" />
                                <!--<label for="state" generated="true" class="error text-red" style="display:none;"></label>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Postal Code</label>
                            <div class="col-sm-8">
                                <input type="text" name="reg[postal_code]" id="postal_code" class="form-control" />
                                <label for="postal_code" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5 control-label">Country<span>*</span></label>
                            <div class="col-sm-8">
                                <select name="reg[country]" id="countryreg" class="form-control">
                                    <option value="">Select Country</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Åland Islands">Åland Islands</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antarctica">Antarctica</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">Bouvet Island</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote D'ivoire">Cote D'ivoire</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Territories">French Southern Territories</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guernsey">Guernsey</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-bissau">Guinea-bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                    <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Isle of Man">Isle of Man</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jersey">Jersey</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                    <option value="Korea, Republic of">Korea, Republic of</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macao">Macao</option>
                                    <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                    <option value="Moldova, Republic of">Moldova, Republic of</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montenegro">Montenegro</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russian Federation">Russian Federation</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Helena">Saint Helena</option>
                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                    <option value="Saint Lucia">Saint Lucia</option>
                                    <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                    <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia">Serbia</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                    <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Timor-leste">Timor-leste</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States">United States</option>
                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Viet Nam">Viet Nam</option>
                                    <option value="Virgin Islands, British">Virgin Islands, British</option>
                                    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                    <option value="Wallis and Futuna">Wallis and Futuna</option>
                                    <option value="Western Sahara">Western Sahara</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                <label for="countryreg" generated="true" class="error text-red" style="display:none;"></label>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="" class="col-sm-5 control-label">Nationality<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[nationality]" id="nationality" class="form-control" />
                            <label for="nationality" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Nature Of Company Business<span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="reg[company_business_nature]" value="" id="company_business_nature" class="form-control" />
                            <label for="company_business_nature" generated="true" class="error text-red" style="display:none;"></label>
                        </div>
                    </div>
                    <div id="britishHajj" style="display:none;">
                        <div class="form-group">
                            <label for="" class="col-sm-9 control-label">Would You Like to become a member of the British Hajj and Umrah Council?</label>
                            <div class="col-sm-9">
                                <div class="radio-custom radio-primary">
                                    <ul>
                                        <li>
                                            <input type="radio" id="british_hajj_umrah_yes" name="reg[british_hajj_umrah_council_membership]" value="1" /> 
                                            <br>
                                            <label for="british_hajj_umrah_yes">Yes</label>
                                        </li>
                                        <li>
                                            <input type="radio" id="british_hajj_umrah_no" name="reg[british_hajj_umrah_council_membership]" value="2" /> 
                                            <br>
                                            <label for="british_hajj_umrah_no">No</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-9 control-label">Would You Like to become a member of the National Pilgrimage Organisers Association?</label>
                            <div class="col-sm-9">
                                <div class="radio-custom radio-primary">
                                    <ul>
                                        <li>
                                            <input type="radio" id="national_pilgrimage_yes" name="reg[national_pilgrimage_organisers_association]" value="1" /> 
                                            <br>
                                            <label for="national_pilgrimage_yes">Yes</label>
                                        </li>
                                        <li>
                                            <input type="radio" id="national_pilgrimage_no" name="reg[national_pilgrimage_organisers_association]" value="2" /> 
                                            <br>
                                            <label for="national_pilgrimage_no">No</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" style="font-size:15px; margin-bottom: 3%;">
                            Social Media ID
                        </label>
                        <div class="col-sm-10" style="margin-bottom: 2%;">
                            <div class="col-sm-2 col-xs-2">
                                <img src="<?= base_url("images/f1.png"); ?>" style="width: 40px; height: 40px;">
                            </div>
                            <div class="col-sm-7 col-xs-10">
                                <input type="text" name="reg[social_id_fb]" id="social_id_fb" class="form-control" />
                                <span class="text-sm">facebook.com/</span>
                            </div>
                        </div>
                        <div class="col-sm-10" style="margin-bottom: 2%;">
                            <div class="col-sm-2 col-xs-2">
                                <img src="<?= base_url("images/t1.png"); ?>" style="width: 40px; height: 40px;">
                            </div>
                            <div class="col-sm-7 col-xs-10">
                                <input type="text" name="reg[social_id_twitter]" id="social_id_twitter" class="form-control" />
                                <span class="text-sm">twitter.com/</span>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-2 col-xs-2">
                                <img src="<?= base_url("images/i1.png"); ?>" style="width: 40px; height: 40px;">
                            </div>
                            <div class="col-sm-7 col-xs-10">
                                <input type="text" name="reg[social_id_instagram]" id="social_id_instagram" class="form-control" />
                                <span class="text-sm">instagram.com/</span>
                            </div>
                        </div>
                    </div>

                    <!--insert multiple-->
                    <div class="form-group">
                        <label for="" class="col-sm-6 control-label">Do you also want to register as</label>
                        <div class="col-sm-8">
                            <div class="checkbox">
                                <label class="control-label"><input type="checkbox" id="check1" name="flag1" value="1" onchange="valueChanged1()">Islamic Tourism Expo</label>
                            </div>
                            <div id="check-flag1" style="display: none;">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 control-label">Participate In<span>*</span></label>
                                    <div class="col-sm-10">
                                        <div class="radio-custom radio-primary">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="London" name="expo[participate_in]" value="London" /> 
                                                    <br>
                                                    <label for="London">London</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Paris" name="expo[participate_in]" value="Paris" /> 
                                                    <br>
                                                    <label for="Paris">Paris</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Cologne" name="expo[participate_in]" value="Cologne" /> 
                                                    <br>
                                                    <label for="Cologne">Cologne</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <label for="expo[participate_in]" generated="true" class="error text-red" style="display:none;"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-5 control-label">Participate As<span>*</span></label>
                                    <div class="col-sm-8">
                                        <div class="radio-custom radio-primary">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="Visitor" name="expo[participate_as]" value="Visitor" /> 
                                                    <br>
                                                    <label for="Visitor">Visitor</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Exhibitor" name="expo[participate_as]" value="Exhibitor" /> 
                                                    <br>
                                                    <label for="Exhibitor">Exhibitor</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Sponsor" name="expo[participate_as]" value="Sponsor" /> 
                                                    <br>
                                                    <label for="Sponsor">Sponsor</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <label for="expo[participate_as]" generated="true" class="error text-red" style="display:none;"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox">
                                <label class="control-label"><input type="checkbox" id="check2" name="flag2" value="2" onchange="valueChanged2()">WHUC Forum</label>
                            </div>
                            <div id="check-flag2" style="display: none;">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 control-label">Participate In<span>*</span></label>
                                    <div class="col-sm-10">
                                        <div class="radio-custom radio-primary">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="Europe" name="forum[participate_in]" value="Europe" /> 
                                                    <br>
                                                    <label for="Europe">Europe</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Africa" name="forum[participate_in]" value="Africa" /> 
                                                    <br>
                                                    <label for="Africa">Africa</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="South_Asia" name="forum[participate_in]" value="South Asia" /> 
                                                    <br>
                                                    <label for="South_Asia">South Asia</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Arabia" name="forum[participate_in]" value="Arabia" /> 
                                                    <br>
                                                    <label for="Arabia">Arabia</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="South_East_Asia" name="forum[participate_in]" value="South East Asia" /> 
                                                    <br>
                                                    <label for="South_East_Asia">South East Asia</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <label for="forum[participate_in]" generated="true" class="error text-red" style="display:none;"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-5 control-label">Participate As<span>*</span></label>
                                    <div class="col-sm-8">
                                        <div class="radio-custom radio-primary">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="Visitor" name="forum[participate_as]" value="Visitor" /> 
                                                    <br>
                                                    <label for="Visitor">Visitor</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Exhibitor" name="forum[participate_as]" value="Exhibitor" /> 
                                                    <br>
                                                    <label for="Exhibitor">Exhibitor</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Sponsor" name="forum[participate_as]" value="Sponsor" /> 
                                                    <br>
                                                    <label for="Sponsor">Sponsor</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <label for="forum[participate_as]" generated="true" class="error text-red" style="display:none;"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox">
                                <label class="control-label"><input type="checkbox" id="check4" name="flag4" value="4" onchange="valueChanged4()">Hajj People Awards</label>
                            </div>
                            <div id="check-flag4" style="display:none;">
                                <div class="form-group">
                                    <label for="" class="col-sm-5 control-label">Participate In<span>*</span></label>
                                    <div class="col-sm-8">
                                        <div class="radio-custom radio-primary">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="London" name="awards[participate_in]" checked="checked" value="Europe" /> 
                                                    <br>
                                                    <label for="London">London</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-5 control-label">Participate As<span>*</span></label>
                                    <div class="col-sm-8">
                                        <div class="radio-custom radio-primary">
                                            <ul>
                                                <li>
                                                    <input type="radio" id="Visitor" name="awards[participate_as]" value="Visitor" /> 
                                                    <br>
                                                    <label for="Visitor">Visitor</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Exhibitor" name="awards[participate_as]" value="Exhibitor" /> 
                                                    <br>
                                                    <label for="Exhibitor">Exhibitor</label>
                                                </li>
                                                <li>
                                                    <input type="radio" id="Sponsor" name="awards[participate_as]" value="Sponsor" /> 
                                                    <br>
                                                    <label for="Sponsor">Sponsor</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <label for="awards[participate_as]" generated="true" class="error text-red" style="display:none;"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--insert multiple-->

                </div>

                <div class="col-md-12 text-center">
                    <div class="alert-error" id="error_server" style="display: none;">

                    </div>
                    <div id="loaderspan" style="display:none;">
                        <img src="<?= base_url('images/loader1.gif'); ?>">
                    </div>
                    <div class="uline" id="success_submit">
                        Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.
                    </div>

                    <input type="submit" id="sbmt" class="btn btn-warning submitBtn" value="Submit" />
                </div>
            </form>
        </div> <!--row-->
    </div> <!--panel body-->
</section>
<script type="text/javascript">
    function valueChanged2()
    {
        if ($('#check2').is(":checked"))
            $("#check-flag2").show();
        else
            $("#check-flag2").hide();
    }
    function valueChanged1()
    {
        if ($('#check1').is(":checked"))
            $("#check-flag1").show();
        else
            $("#check-flag1").hide();
    }
    function valueChanged4()
    {
        if ($('#check4').is(":checked"))
            $("#check-flag4").show();
        else
            $("#check-flag4").hide();
    }
</script>
<script type="text/javascript">
    $("#mobile_no").intlTelInput();
    $('select[id="countryreg"]').change(function () {
        if ($(this).val() == "United Kingdom") {
            $("#britishHajj").show();
        } else {
            $("#britishHajj").hide();
        }
    });

    function formvalidation() {
        var validator = $("#regForm").validate({
            rules: {
                "reg[participate_in]": {
                    required: true
                },
                "reg[participate_as]": {
                    required: true
                },
                "reg[company_name]": {
                    required: true
                },
                "reg[contact_name]": {
                    required: true
                },
                "reg[job_title]": {
                    required: true
                },
                "reg[mobile_no]": {
                    required: true
                },
                "reg[email]": {
                    required: true
                },
                "reg[gender]": {
                    required: true
                },
                "reg[company_business_nature]": {
                    required: true
                },
                "reg[nationality]": {
                    required: true
                },
                "reg[country]": {
                    required: true
                },
                "reg[city]": {
                    required: true
                },
                "reg[address_line1]": {
                    required: true
                },
                "expo[participate_in]" : {
                    required: true
                },
                "expo[participate_as]" : {
                    required: true
                },
                "forum[participate_in]" : {
                    required: true
                },
                "forum[participate_as]" : {
                    required: true
                },
                "awards[participate_as]" : {
                    required: true
                }
            },
            messages: {
                "reg[participate_in]": {
                    required: "This field is required."
                },
                "reg[participate_as]": {
                    required: "This field is required."
                },
                "reg[company_name]": {
                    required: "This field is required.",
                },
                "reg[contact_name]": {
                    required: "This field is required."
                },
                "reg[job_title]": {
                    required: "This field is required."
                },
                "reg[mobile_no]": {
                    required: "This field is required."
                },
                "reg[email]": {
                    required: "This field is required.",
                    email: "Enter an valid email."
                },
                "reg[gender]": {
                    required: "This field is required."
                },
                "reg[company_business_nature]": {
                    required: "This field is required."
                },
                "reg[nationality]": {
                    required: "This field is required."
                },
                "reg[country]": {
                    required: "This field is required."
                },
                "reg[city]": {
                    required: "This field is required."
                },
                "reg[address_line1]": {
                    required: "This field is required."
                },
                "expo[participate_in]" : {
                    required: "This field is required."
                },
                "expo[participate_as]" : {
                    required: "This field is required."
                },
                "forum[participate_in]" : {
                    required: "This field is required."
                },
                "forum[participate_as]" : {
                    required: "This field is required."
                },
                "awards[participate_as]" : {
                    required: "This field is required."
                }
            }
        });
        var x = validator.form();
        if (x) {
            $("#sbmt").prop('disabled', true);
            $('#loaderspan').show();
            return true;
        } else {
            $("#sbmt").prop('disabled', false);
            return false;
        }
    }
    function calbackFun(result) {
        $('#loaderspan').hide();
        var data = JSON.parse(result);
        if (data.hasError == true) {
            if (data.servermessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.servermessage + "</h4>");
                return false;
            } else if (data.methodmessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.methodmessage + "</h4>");
                return false;
            } else if (data.existsmessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.existsmessage + "</h4>");
                $("#sbmt").prop('disabled', false);
                return false;
            }
        } else {
            $("#sbmt").prop('disabled', false);
            $('#success_submit').show();
            setTimeout(function () {
                $('#success_submit').hide();
                window.location.href = "" + data.redirecturl + "";
            }, 10000);
            return false;
        }
    }
</script>

<?php
include SITE_ROOT . 'views/regFooter.php';
?>

