<?php

class Email extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('adminmodel');
        $this->load->model('usermodel');
        $this->load->helper('email');
        $this->load->library('form_validation');
    }

    public function sendMail($flg = "") {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $toemail = "";
            $captcha = $this->input->post('captcha');
            //@$sessioncaptcha = $this->session->userdata('captchaMath');
            @$sessioncaptcha = $this->session->userdata('captchaWord');
            
            //server validation
            $this->form_validation->set_rules('contactus[name]', 'Name', 'required');
            $this->form_validation->set_rules('contactus[email]', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('contactus[phone]', 'Phone', 'required');
            $this->form_validation->set_rules('contactus[subject]', 'Subject', 'required');
            $this->form_validation->set_rules('contactus[message]', 'Message', 'required');
            if ($this->form_validation->run() == FALSE) {
                $message = "";
                if (form_error('contactus[name]')) {
                    $message = "Name is required.";
                } else if (form_error('contactus[email]')) {
                    $message = "Email is required.";
                } else if (form_error('contactus[phone]')) {
                    $message = "Phone is required.";
                } else if (form_error('contactus[subject]')) {
                    $message = "Subject is required.";
                } else if (form_error('contactus[message]')) {
                    $message = "Message is required.";
                }
                $res['hasError'] = true;
                $res['servermessage'] = $message;
                echo json_encode($res);
                exit();
            }
            if ($captcha != $sessioncaptcha) {
                $res['hasError'] = true;
                $res['captchamessage'] = "Captcha Error";
                echo json_encode($res);
                exit();
            }
            if ($this->input->post('que')['name']) {
                $toemail_ar = $this->adminmodel->detail("settings", "set_key", "SSTA_QUERY_EMAIL");
            } else if ($this->input->post('contactus')['name']) {
                $toemail_ar = $this->adminmodel->detail("settings", "set_key", "SSTA_CONTACTUS_EMAIL");
            }
            $to_email = $toemail_ar['set_value'];
            $name = $this->input->post('que')['name'] ? $this->input->post('que')['name'] : $this->input->post('contactus')['name'];
            $email = $this->input->post('que')['email'] ? $this->input->post('que')['email'] : $this->input->post('contactus')['email'];
            $phone = $this->input->post('que')['phone'] ? $this->input->post('que')['phone'] : $this->input->post('contactus')['phone'];
            if ($flg == 1) {
                $subject = "Query : from Getonboard";
                $question = $this->input->post('que')['message'];
                $td_header = "Question";
            } else {
                $subject = $this->input->post('contactus')['subject'] . ": from Getonboard";
                $question = $this->input->post('contactus')['message'];
                $td_header = "Message";
            }
            $message = "<html>
<body>
<table width='40%' cellpadding='0' cellspacing='0' border='0' id='background-table' >
    <tr>
        <td>Name : </td>
        <td>" . $name . "</td>
    </tr>
    <tr>
        <td>Email : </td>
        <td>" . $email . "</td>
    </tr>
    <tr>
        <td>" . $td_header . " : </td>
        <td>" . $question . "</td>
    </tr>
</table>
</body>
</html>";

            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "metro706.hostmetro.com";
            $config['smtp_port'] = "587";
            $config['smtp_user'] = "smtp@skycobalt.com";
            $config['smtp_pass'] = "smtp@143";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $ci->email->initialize($config);

            $ci->email->from($email, $name);
            $list = explode(",", $to_email);
            $ci->email->to($list);
            $this->email->reply_to($email, $name);
            $ci->email->subject($subject);
            $ci->email->message($message);
            $mail = $this->email->send();
            if ($flg) {
                return $mail;
            } else {
                $res['hasError'] = false;
                $res['successmessage'] = "Mail sent successfully.";
                echo json_encode($res);
//                print "success";
                exit;
            }
        } else {
            $res['hasError'] = true;
            $res['methodmessage'] = "Internal Server Error";
        }
        echo json_encode($res);
        exit();
    }

    public function sendRequestSolutionMail($solution) {
        $toemail_ar = $this->adminmodel->detail("settings", "set_key", "SSTA_CONTACTUS_EMAIL");
        $to_email = $toemail_ar['set_value'];
        $subject = "Request for solution : from Getonboard";
        $message = "<html>
<body>
<table width='40%' cellpadding='0' cellspacing='0' border='0' id='background-table' >
    <tr>
        <td>Name : </td>
        <td>" . $solution['name'] . "</td>
    </tr>
    <tr>
        <td>Phone : </td>
        <td>" . $solution['phone'] . "</td>
    </tr>
    <tr>
        <td>Description : </td>
        <td>" . $solution['description'] . "</td>
    </tr>
</table>
</body>
</html>";

        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "metro706.hostmetro.com";
        $config['smtp_port'] = "587";
        $config['smtp_user'] = "smtp@skycobalt.com";
        $config['smtp_pass'] = "smtp@143";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);

        $ci->email->from($solution['email'], $name);
        $list = explode(",",$to_email);
        $ci->email->to($list);
        $this->email->reply_to($solution['email'], $name);
        $ci->email->subject($subject);
        $ci->email->message($message);
        return $this->email->send();
    }

    public function sendQuestionMail($que) {
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "metro706.hostmetro.com";
        $config['smtp_port'] = "587";
        $config['smtp_user'] = "smtp@skycobalt.com";
        $config['smtp_pass'] = "smtp@143";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $staff = ($que['staff']>500)?"500+":$que['staff'];
        $tickets = ($que['tickets']>500)?"500+":$que['tickets'];
        //User Detail
        $name = $this->session->userdata('session_data')['name'];
        $email = $this->session->userdata('session_data')['email'];
        $phone = $this->session->userdata('session_data')['phone'];
        $agency_name = $this->session->userdata('session_data')['agency_name'];
        $address = $this->session->userdata('session_data')['address'];
        $city = $this->session->userdata('session_data')['city'];
        $country = $this->session->userdata('session_data')['country'];

        $toemail_ar = $this->adminmodel->detail("settings", "set_key", "SSTA_QUESTIONNAIRE_EMAIL");
        //print_r($toemail_ar);
        $to_email = $toemail_ar['set_value'];
        //print_r($to_email);
        $subject = "Getonboard Questionnaire ";
        $results = "";

        if ($que['fulfillment']['flg'] == 'yes') {
            $agencyId = implode(",", $que['fulfillment']['agency']);
            @$agencyList = $this->usermodel->agencyList($agencyId);
            if (!empty($agencyList)) {
                foreach ($agencyList as $k => $v) {
                    $results .= "<li>$v</li>";
                }
            }
        } else {
            $results = "No Agencies are there";
        }

        $message = "<html>
<body>
<div><b>User Detail:</b></div>
<table width='50%' cellpadding='0' cellspacing='0' border='0' id='background-table' >
    <tr>
        <td>Name : </td>
        <td>" . $name . "</td>
    </tr>
    <tr>
        <td>Email : </td>
        <td>" . $email . "</td>
    </tr>
    <tr>
        <td>Phone : </td>
        <td>" . $phone . "</td>
    </tr>
    <tr>
        <td>Agency Name : </td>
        <td>" . $agency_name . "</td>
    </tr>
</table>
<div><b>Questionnaire Detail:</b></div>
<table width='50%' >
            <tr>
                <td>1.Are you IATA certified travel agency?</td>
                <td>:</td>
                <td>" . $que['IATA'] . "</td>
            </tr>
            <tr>
                <td>2.How many staff do you have?</td>
                <td>:</td>
                <td>" . $staff . "</td>
            </tr>
            <tr>
                <td>3.How many Air tickets do you sell/intend to sell per month?</td>
                <td>:</td>
                <td>" . $tickets . "</td>
            </tr>
            <tr>
                <td>4.Do you work with any GDS today?</td>
                <td>:</td>
                <td>" . $que['GDS'] . "</td>
            </tr>

            <tr>
                <td>5.Do you issue your own tickets?</td>
                <td>:</td>
                <td>" . $que['own_tickets'] . "</td>
            </tr>
            <tr>
                <td>6.Do you have current preferred ticketing partners?</td>
                <td>:</td>
                <td>" . $que['ticket_partner'] . "</td>
            </tr>
            <tr>
                <td>7. Would you like to have any of our fulfilment agencies?</td>
                <td>:</td>
                <td>" . $que['fulfillment']['flg'] . "</td>
            </tr>
            <tr>
                    <td>Agencies are</td>
                    <td>:</td>
                    <td>
                        <ul>" . $results . "</ul>
                    </td>
                </tr>
            <tr>
                <td>8.Do you sell / intend to sell hotels?</td>
                <td>:</td>
                <td>" . $que['hotels'] . "</td>
            </tr>
            <tr>
                <td>9.Do you have your own company e-mail/domain?</td>
                <td>:</td>
                <td>" . $que['domain'] . "</td>
            </tr>
            <tr>
                <td>10.Do you have an accounting solution?</td>
                <td>:</td>
                <td>" . $que['act_solution'] . "</td>
            </tr>
            <tr>
                <td>11.Do you have a website?</td>
                <td>:</td>
                <td>" . $que['question_website'] . "</td>
            </tr>
        </table>
</body>
</html>";

        $ci->email->initialize($config);

        $ci->email->from($email, $name);
       
        $list = explode(",",$to_email);
       // print_r($list);
        $ci->email->to($list);
        $this->email->reply_to($email, $name);
        $ci->email->subject($subject);
        $ci->email->message($message);
        return $this->email->send();
    }

    public function loginDetail() {
        $to_email = $this->input->post('email');
        //server side validation
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $message = "";
            if(form_error('email')){
               $message = "Email is required.";
           }
           $res['hasError'] = true;
           $res['servermessage'] = $message;
           echo json_encode($res);
           exit();
        }
        
        $res = $this->usermodel->loginDetail($to_email);
        if (!empty($res)) {
            $subject = "Login Details";
            $password = $res[0]['password'];
            $message = "<html>
                    <body>
                    <table width='40%' cellpadding='0' cellspacing='0' border='0' id='background-table' >
                        <tr>
                            <td>Email : </td>
                            <td>" . $to_email . "</td>
                        </tr>
                        <tr>
                            <td>Password : </td>
                            <td>" . $password . "</td>
                        </tr>
                    </table>
                    </body>
                    </html>";
            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "metro706.hostmetro.com";
            $config['smtp_port'] = "587";
            $config['smtp_user'] = "smtp@skycobalt.com";
            $config['smtp_pass'] = "smtp@143";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $ci->email->initialize($config);

            $ci->email->from('salesdxb@amadeus.ae', 'hramadeus');
            $list = explode(",",trim($to_email));
            $ci->email->to($list);
            $ci->email->subject($subject);
            $ci->email->message($message);
            $this->email->send();
            $res['hasError'] = false;
            $res['responsemessage'] = "success";
//            print "success";
        } else {
            $res['hasError'] = true;
            $res['responsemessage'] = "Invalid User";
//            print "Invalid User";
        }
        echo json_encode($res);
        exit();
    }

    function testemail() {
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "metro706.hostmetro.com";//"lnx25.securehostdns.com";
        $config['smtp_port'] = "587";//587
        $config['smtp_user'] = "smtp@skycobalt.com";//"smtp@thoughtspheres.com";
        $config['smtp_pass'] = "smtp@143";//"smtp@#$2016";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);

        $ci->email->from('sachinmore16@gmail.com', 'Sachin');
        $list = array('sahoo.purusottama@gmail.com');
        $ci->email->to($list);
//        $this->email->reply_to('<HERE COMES YOUR EMAIL>', 'Explendid Videos');
        $ci->email->subject('This is an email test');
        $ci->email->message("<div style='text-align:center;'>Hi <b>Sachin</b>!</div");
        if ($this->email->send()) {
            echo "you are luck!";
        } else {
            echo $this->email->print_debugger();
        }
    }
}