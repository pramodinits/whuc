<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('adminmodel');
        $this->load->library('pagination');
        $this->load->helper('download');
        $this->load->library('form_validation');
        $this->load->helper('email');
    }
    

    public function setmysession() {
        $the_session = array("key1" => "value1", "key2" => "value2");
        $this->session->set_userdata($the_session);
        print "<pre>";
        $ss = $this->session->userdata();
        print_r($ss);
    }

    public function getmysession() {
        $ss = $this->session->userdata();
        print "<pre>";
        print_r($ss);
    }

    public function index() {
       
        $this->load->view("user/homePage");
    }
//static page

public function privacy() {
        $this->load->view("user/privacy");
    }

public function termsUse() {
        $this->load->view("user/termsUse");
    }

public function cookies() {
        $this->load->view("user/cookies");
    }

public function contactus() {
        $this->load->view("user/contactus");
    }

public function aboutus() {
        $this->load->view("user/aboutus");
    }



    

    /**
     * For WHUC admin registration
     */
    public function registration() {
        $data = array();
        $data['logo'] = "images/reg-logo.png";
        $data['title'] = "HAJJ People Islamic Tourism ";
        $this->load->view("user/registrationIslamicTourism",$data);
    }
    public function registrationForum() {
         //echo  exit();
       $data = array();
        $data['logo'] = "images/forum-reg-logo.png";
        $data['title'] = "WHUC Forum";
        $this->load->view("user/registrationForum",$data);
    }
    public function registrationDixie() {
         $data = array();
       $data['logo'] = "images/dixie.png";
         $data['title'] = "Dixie Queen";
        $this->load->view("user/registrationDixie",$data);
    }
    public function registrationAwards() {
         $data = array();
        $data['logo'] = "images/hajj.png";
         $data['title'] = "Hajj People Awards";
        $this->load->view("user/registrationAwards",$data);
    }

    public function insertregUser() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $reg = $this->input->post('reg');
//            $reg['dob'] = $this->input->post('dob');
            $reg['dob'] = date('Y-m-d', strtotime($this->input->post('dob')));
            $reg['umrah_pilgrims_annually'] = !empty($this->input->post('umrah_pilgrims_annually')) ? $this->input->post('umrah_pilgrims_annually') : 0;
            $reg['hajj_pilgrims_annually'] = !empty($this->input->post('hajj_pilgrims_annually')) ? $this->input->post('hajj_pilgrims_annually') : 0;
            $profile_img = str_replace(' ', '_', $_FILES['profile_img']['name']);
            $reg['profile_img'] = $profile_img;
            $reg['add_date'] = date("Y-m-d H:i:s");
            $reg['ip'] = $_SERVER['REMOTE_ADDR'];
            $img_id = "";

            //insert record
            $ins_id = $this->adminmodel->insertData('registration_user', $reg);
            $img_id = $ins_id;

            if (!empty($_FILES['profile_img']['name'])) {
                $source = $_FILES['profile_img']['tmp_name'];
                $dest = IMG_ROOT . "images/" . $img_id . "_" . $profile_img;
                @copy($source, $dest);
            }
            //send email
            
            $contentMail = 'Dear ' . $reg["contact_name"] . ', <br/><br/>'
                    . 'Thank you for registering with us.';
            /* core email */
            $subject = "User Registration";
            $body = $contentMail;
            $to = $reg['email'];
            $cc = $adminEmail = $this->config->item('adminemail');
            $this->sendEmail($subject, $body, $to, $cc);
            
            
            /* core email */
            /*
            require('PHP-Email/class.phpmailer.php');
            $adminEmail = $this->config->item('adminemail');
            $mail = new PHPMailer();
            $subject = "User Registration";
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = TRUE;
            $mail->SMTPSecure = "tls";
            $mail->Port = SMTPPORT;
            $mail->Username = SMTPUSERNAME;
            $mail->Password = SMTPPASSWORD;
            $mail->Host = SMTPHOST;
            $mail->Mailer = "smtp";
            $mail->From = SMTPFROMEMAIL;
            $mail->FromName = SMTPFROMNAME;
            $mail->AddAddress($reg['email']);
            //$mail->AddCC("sahoo.purusottama@gmail.com");
            $mail->AddCC($adminEmail);
            $mail->Subject = $subject;
            $mail->WordWrap = 80;
            $mail->MsgHTML($contentMail);
            $mail->IsHTML(true);
            $mail->Send();
            //send email
            */
            $res['hasError'] = false;
            $res['redirecturl'] = base_url();
        } else {
            $res['hasError'] = true;
            $res['methodmessage'] = "Internal Server Error";
        }
        echo json_encode($res);
        exit();
    }
    public function testEmail(){
        $subject = "Test Test ";
        $body = "Body";
        $to = "sahoo.purusottama@gmail.com";
        $cc = "sales@skycobalt.com,smore@skycobalt.com";
        echo $this->sendEmail($subject,$body,$to,$cc);
    }
     public function sendEmail($subject, $body, $to, $cc, $filepath = null) {
        $config = Array(
            'protocol' => 'smtp',
            //'smtp_host' => 'lnx25.securehostdns.com',
            // 'smtp_host' => 'ssl://smtp.zoho.com',
            'smtp_host' => 'metro706.hostmetro.com',
            'smtp_port' => 587,
            //'smtp_port' => 465,
            'smtp_user' => 'smtp@skycobalt.com',
            'smtp_pass' => 'smtp@143',
            'mailtype' => 'html',
                //'smtp_user' => 'alert@iliftserviceapp.com',
                //'smtp_pass' => 'Ali#241@dkj'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('event@hajjpeople.com', 'Info');
        if ($to) {
            $this->email->to($to);
        }

        if ($cc) {
            $this->email->cc($cc);
        }
        if ($filepath) {
            $this->email->attach($filepath);
        }

        $this->email->subject($subject);
        $this->email->message($body);
        return $this->email->send();

        return true;
//        if (!$this->email->send()) {
//            show_error($this->email->print_debugger());
//        } else {
//            echo 'Your e-mail has been sent!';
//        }
    }


}
