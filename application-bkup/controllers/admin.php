<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'email'));
        $this->load->model('adminmodel');
        $this->load->model('usermodel');
        //$this->load->model('productmodel');
        $this->load->library('pagination');
        $this->load->library('session');
        $this->load->library('form_validation');
    }

    /* By default in the Admin Sight to Show the Admin Login page */

    public function index() {
        $session_data = $this->session->userdata('session_data');
        if ($session_data) {
            redirect(base_url('admin/dashboard'));
        } else {
            $this->load->view("admin/loginForm");
        }
    }

    public function login() {
        $session_data = $this->session->userdata('session_data');
        if ($session_data) {
            redirect(base_url('admin/dashboard'));
        } else {
            $this->load->view("admin/loginForm");
        }
    }

    public function setLogin($username = "", $password = "") {
        $this->form_validation->set_rules('username', 'User Name', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $message = "";
            if (form_error('username')) {
                $message = "Enter valid email.";
            } else if (form_error('password')) {
                $message = "Password is required.";
            }
            $res['hasError'] = true;
            $res['errormessage'] = $message;
            echo json_encode($res);
            exit();
        } else {
            $username = $this->input->post('username') ? $this->input->post('username') : $username;
            $password = $this->input->post('password') ? $this->input->post('password') : $password;
            $result = $this->adminmodel->loginAdmin($username);
            if (!empty($result)) {
                if (($username == $result['email']) && ($password == $result['password'])) {
                    $this->session->set_userdata('session_data', $result);
                    $this->session->set_flashdata('message', 'Sucessfully logged in.');
                    $res['hasError'] = false;
                    $res['redirecturl'] = base_url('admin/dashboard');
                    echo json_encode($res);
                    exit;
                } else {
                    $res['hasError'] = true;
                    $res['errormessage'] = "Invalid User Name or Password";
                    echo json_encode($res);
                    exit();
                }
            } else {
                $res['hasError'] = true;
                $res['errormessage'] = "Invalid User Name or Password";
                echo json_encode($res);
                exit();
            }
        }
    }

    public function loggedOut() {
        $this->session->unset_userdata('session_data');
        session_decode();
        redirect(base_url('admin/login'));
    }

    private function checkLogin() {
        $session_data = $this->session->userdata('session_data');
        if (empty($this->session->userdata('session_data'))) {
            redirect(base_url('admin/login'));
        } else {
            return $session_data['user_type'];
        }
    }

    public function dashboard() {
        $chklogin = $this->checkLogin();
        if ($chklogin) {
            $cond = "(email_verify = 1 or email_verify is NULL)";
            $data['userCountFlag1'] = $this->usermodel->rowCount('registration_user', 'flag', '1', $cond);
            $data['userCountFlag2'] = $this->usermodel->rowCount('registration_user', 'flag', '2', $cond);
            $data['userCountFlag3'] = $this->usermodel->rowCount('registration_user', 'flag', '3', $cond);
            $data['userCountFlag4'] = $this->usermodel->rowCount('registration_user', 'flag', '4', $cond);
            $this->load->view('admin/dashboard', $data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function userListingVerified($flag = "") {
        $chklogin = $this->checkLogin();
        if ($chklogin) {
            $cond = "(email_verify = 1 or email_verify is NULL)";
            $data['userList'] = $this->usermodel->userListing($cond, $flag);
            //set session
            $session_data = $this->session->userdata('session_data');
            $session_data['redirecturl'] = $_SERVER['REQUEST_URI']; 
            $this->session->set_userdata("session_data", $session_data);
            $this->load->view('admin/userListingVerified', $data);
        } else {
            redirect(base_url('admin/login'));
        }
    }
    
    public function userListingUnverified($cond = "") {
        $chklogin = $this->checkLogin();
        if ($chklogin) {
            $cond = "email_verify != 1";
            $data['userList'] = $this->usermodel->userListing($cond);
             //set session
            $session_data = $this->session->userdata('session_data');
            $session_data['redirecturl'] = $_SERVER['REQUEST_URI']; 
            $this->session->set_userdata("session_data", $session_data);
            $this->load->view('admin/userListingUnverified', $data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function viewUser() {
        $chklogin = $this->checkLogin();
        if ($chklogin) {
            $reg_id = $this->input->get('reg_id');
            $data['userDetail'] = $this->adminmodel->detail("registration_user", "reg_id", $reg_id);
            $data['redirecturl'] = $this->session->userdata('session_data')['redirecturl'];
            $this->load->view('admin/viewUser', $data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function userEdit() {
        $this->checkLogin();
        $reg_id = $this->input->get('reg_id');
        $flag = $this->input->get('flag');
        $cond = "flag =" . $flag;
        $data['userDetail'] = $this->adminmodel->detail("registration_user", "reg_id", $reg_id, $cond);
        $data['redirecturl'] = $this->session->userdata('session_data')['redirecturl'];
        if ($data['userDetail']) {
            $this->load->view('admin/userEdit', $data);
        } else {
            $this->load->view('admin/userListing');
        }
    }

    public function editUser() {
        $this->checkLogin();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $reg = $this->input->post('reg');
            
            $reg_id = $this->input->post('reg_id');
            if ($reg_id) {
//                $reg['dob'] = $this->input->post('birth_date');
                $reg['umrah_pilgrims_annually'] = !empty($this->input->post('umrah_pilgrims_annually')) ? $this->input->post('umrah_pilgrims_annually') : 0;
                $reg['hajj_pilgrims_annually'] = !empty($this->input->post('hajj_pilgrims_annually')) ? $this->input->post('hajj_pilgrims_annually') : 0;
                //update record
                $upd_id = $this->adminmodel->updateData('registration_user', $reg, 'reg_id', $reg_id);
                $res['hasError'] = false;
                $res['redirecturl'] = base_url('admin/userListingVerified');
            } else {
                $res['hasError'] = true;
                $res['methodmessage'] = "Invalid parameter";
            }
        } else {
            $res['hasError'] = true;
            $res['methodmessage'] = "Internal Server Error";
        }
        echo json_encode($res);
        exit();
    }
    
    public function deleteUser() {
       $chklogin = $this->checkLogin();
        $reg_id = $this->input->post('reg_id');
        if ($chklogin && $reg_id) {
            $condition = "WHERE reg_id = $reg_id";
            $result = $this->adminmodel->deleteData('registration_user', $condition);
            echo "Success";
        } else {
            echo "Error";
        }
    }
    
    public function deleteAllChecked() {
        $chklogin = $this->checkLogin();
        if ($chklogin) {
        $id_users = $this->input->post('deleteUsers');
        $redirecturl = $this->input->post('redirecturl');
        $condition = "WHERE reg_id IN (".implode(",", $id_users ) . ")";
        $result = $this->adminmodel->deleteData("registration_user", $condition);
        $this->session->set_flashdata('message', 'Record successfully deleted.');
        redirect(base_url('admin/'.$redirecturl));
        } else {
            redirect(base_url('admin/login'));
        }
    }

}

?>
