<?php

class Usermodel extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function userListing($cond = "", $flag = "") {
        $data = array();
        $this->db->select('*');
        $this->db->from('registration_user');
        if($cond){
            $this->db->where($cond);
        }
        if($flag){
            $this->db->where('flag', $flag);
        }
        $this->db->order_by('reg_id', 'DESC');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

    
    function rowCount($tbl, $column_name, $column_value, $cond="") {
        $data = array();
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($column_name, $column_value);
        if ($cond){
            $this->db->where($cond);
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->num_rows();
        } else {
            $data = 0;
        }
        return $data;
    }

    function loginDetail($email) {
        $data = array();
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email LIKE', $email);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

}
