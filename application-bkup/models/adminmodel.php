<?php

class Adminmodel extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /* function for  checking and retriving User_ID, Login_Name and Password to Login */

    function loginAdmin($username = "", $userId = "") {
        $data = array();
        $this->db->select("*,CONCAT(fname,' ',lname) AS `name`", FALSE);
        $this->db->from('user');
        if (!empty($username)) {
            $this->db->where('email = ' . "'" . $username . "'");
        }
        if (!empty($userId)) {
            $this->db->where('id_user = ' . "'" . $userId . "'");
        }
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        } $query->free_result();
        return $data;
    }

    function insertData($tbl, $userData) {
        $this->db->insert($tbl, $userData);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function updateData($tbl, $userData, $field_name, $field_val) {
        $this->db->where($field_name, $field_val);
        $upd = $this->db->update($tbl, $userData);
    }
    
    function deleteData($table, $condition) {
        $this->db->query("DELETE FROM $table $condition");
    }

    function detail($tbl, $fieldName, $fieldVal, $cond = "") {
        $data = array();
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where("$fieldName = ", $fieldVal);
        if ($cond) {
            $this->db->where($cond);
        }
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        }
        return $data;
    }
    
    function record_exist($fld_name, $fld_val, $tbl, $cond = '') {
        $data = array();
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where("$fld_name = ", $fld_val);
        if (!empty($cond)) {
            $this->db->where($cond);
        }
        $this->db->limit(1);

        $query = $this->db->get();
        $row_cnt = $query->num_rows();
        return $row_cnt;
    }
}