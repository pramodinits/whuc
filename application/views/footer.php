<!-- Footer
                ============================================= -->
<footer id="footer" class="dark" style="background-color: #000;">

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">
            <div class="col-md-4 nobottommargin" style="color: #A9A9A9;">
                &copy; <?= date('Y'); ?> Hajj People. All rights reserved.
            </div>
            <div class="col-md-5">
                <ul class="footer-menu">
                    <li>
                        <a href="<?= base_url('privacy'); ?>">Privacy</a>
                    </li>
                    <li>
                        <a href="<?= base_url('terms-of-Use'); ?>">Terms of Use</a>
                    </li>
                    <li>
                        <a href="<?= base_url('cookies'); ?>">Cookies</a>
                    </li>
                    <li>
                        <a href="<?= base_url('contactus'); ?>">Contact Us</a>
                    </li>
                    <li>
                        <a href="<?= base_url('aboutus'); ?>">About Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="nobottommargin col_last tright" style="font-size: .9em;">
                    <div class="fright clearfix">
                        <a href="https://www.facebook.com/hajjpeople09" target="_blank" class="social-icon si-small si-borderless">
                            <img src="<?= base_url('images/f1.png'); ?>">
                        </a>
                        <a href="https://www.twitter.com/hajjpeople_whuc" target="_blank" class="social-icon si-small si-borderless">
                            <img src="<?= base_url('images/t1.png'); ?>">
                        </a>

                        <a href="https://www.instagram.com/official.whuc" target="_blank" class="social-icon si-small si-borderless">
                            <img src="<?= base_url('images/i1.png'); ?>">
                        </a>
                    </div>

                </div>
            </div>
        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<!--<script type="text/javascript" src="<?//= base_url('js/jquery.js');?>"></script>-->
<script type="text/javascript" src="<?= base_url('js/plugins.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('js/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('js/ajaxupload.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="<?= base_url('js/functions.js'); ?>"></script>
<!-- Number Slider JavaScripts
============================================= -->
<script>
    $(document).ready(function () {
        $('#myModal').modal('show');
        setTimeout(function () {
            $('#myModal').modal('hide');
        }, 5000);
    });
    function showModal() {
       $('#myModal').modal('show'); 
    }
</script>
<script>
    $('.alert').fadeIn('fast').delay(4000).fadeOut('fast');
    </script>

</body>
</html>