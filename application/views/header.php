<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="SemiColonWeb" />

        <!-- Stylesheets
        ============================================= -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/swiper.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/dark.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/font-icons.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/animate.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/switcher.css'); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('css/magnific-popup.css'); ?>" type="text/css" />

        <link rel="stylesheet" href="<?php echo base_url('css/responsive.css'); ?>" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lt IE 9]>
                <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

        <!-- Document Title
        ============================================= -->
        <title>WHUC</title>
        <script type="text/javascript" src="<?php echo base_url('js/jquery.js'); ?>"></script>
        <script src="<?= base_url('js/jquery.mask.js'); ?>"></script>
        <style>
            .alert {
                display: none;
            }
            .alert-success, .alert-danger, .alert-info {
                text-align: center;
            }
        </style>
    </head>

    <body class="stretched">

        <!-- Document Wrapper
        ============================================= -->
        <div id="wrapper" class="clearfix">

            <!-- Header
            ============================================= -->
            <header id="header">

                <div id="header-wrap">

                    <div class="container clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <div id="logo">
                                    <a href="<?= base_url(); ?>" class="standard-logo" data-dark-logo="<?php echo base_url('images/logo.png'); ?>">
                                        <img src="<?php echo base_url('images/logo.png'); ?>" style="margin-top: 8%;margin-bottom: 8%;height: 80px;"alt="Amadeus Logo"></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                <?php } else if ($this->session->flashdata('error')) { ?>
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                <?php } else if ($this->session->flashdata('info')) { ?>
                                    <div class="alert alert-info">
                                        <?php echo $this->session->flashdata('info'); ?>
                                    </div>
                                <?php }
                                ?>
                            </div>
                            <div class="col-md-3" style="margin-top:3%; text-align: right;">
                                <span style="font-size: 14px; color: #969696;">
                                    London, UK
                                </span><br>
                                <span style="font-size: 18px; color: #5d1a6e;">
                                    3<sup>rd</sup> - 5<sup>th</sup> November 2019
                                </span>
                            </div>
                        </div>


                        <!-- Logo
                        ============================================= -->
                        <!-- #logo end -->



                    </div>
                </div>

            </header><!-- #header end -->
<div style="clear:both;"></div>