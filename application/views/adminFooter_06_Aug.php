<!--datatable footer-->
<script type="text/javascript" src="<?= base_url(); ?>assets/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/datatable/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/datatable/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/datatable/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/datatable/js/responsive.bootstrap.min.js"></script>

<!--datatable footer-->
<!--Start Message Notification on header part -->
<script>
// A $( document ).ready() block.
    var notificationurl = "<?= base_url('admin/getnotification');?>"
    $(document).ready(function () {
       // alert(notificationurl);
        getmessageNotification();
    });
    function getmessageNotification() {
        var jqxhr = $.get(notificationurl, function (result) {
            //alert(result);
            var res = $.parseJSON(result)            
            if(res.HasError){
                alert(res.ErrorMessage);
                //location.reload();
            }else{
                var messageresult = res.result;
               // alert(messageresult+"++"+messageresult.count);
                $('#message_notification').html(messageresult.count);
                if(messageresult.count){
                    //$('#message_notification_div').show();
                    var str ="";
                    for(var i in messageresult.data){                       
                       str += "<li>";
                       str += "<a href='#' class='clearfix'>";
                       str += "<span class='title'>"+messageresult.data[i].agency_name+"</span>";
                       str += "<span class='message'>"+messageresult.data[i].subject+"</span>";
                       str += "</a></li>"; 
                       if(i == 2){
                           break;
                       }
                    }
                    $('#message_notification_body').html(str);
                    
                }else{
                    $('#message_notification').hide();
                    $('#message_notification_div').hide(); 
                }
            }
            //if(res.has)
        });
        //setInterval(function(){ getmessageNotification() }, 30000);
    }
</script>
<!--End Message Notification on header part -->

