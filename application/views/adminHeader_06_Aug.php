<?php $method = $this->router->fetch_method(); ?>
<!doctype html>
<html class="boxed">
    <head>

        <!-- Basic -->
        <meta charset="UTF-8">

        <title>Dashboard</title>
        <meta name="keywords" content="HTML5 Admin Template" />
        <meta name="description" content="Porto Admin - Responsive HTML5 Template">
        <meta name="author" content="okler.net">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/vendor/font-awesome/css/font-awesome.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap-datepicker/css/datepicker3.css'); ?>" />

        <!--datatable css-->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/datatable/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/datatable/css/fixedHeader.bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/datatable/css/responsive.bootstrap.min.css">
        <!--datatable css-->

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/stylesheets/theme.css'); ?>" />

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/stylesheets/skins/default.css'); ?>" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/stylesheets/theme-custom.css'); ?>">

        <!-- Head Libs -->
        <script src="<?= base_url('assets/vendor/modernizr/modernizr.js'); ?>"></script>
        <!-- Vendor -->
        <script src="<?= base_url('assets/vendor/jquery/jquery.js'); ?>"></script>
        <script src="<?= base_url('js/jquery.mask.js'); ?>"></script>
        <script src="<?= base_url('js/jquery.validate.js'); ?>"></script>
        <script src="<?= base_url('js/ajaxupload.js'); ?>"></script>
        <script src="<?= base_url('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js'); ?>"></script>
        <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
        <script src="<?= base_url('assets/vendor/nanoscroller/nanoscroller.js'); ?>"></script>
        <script src="<?= base_url('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
        <script src="<?= base_url('assets/vendor/magnific-popup/magnific-popup.js'); ?>"></script>
        <script src="<?= base_url('assets/vendor/jquery-placeholder/jquery.placeholder.js'); ?>"></script>

        <style>
            /*            section.body {
                            margin: 0 !important;
                        }*/
            html.boxed .body {
                max-width: 98% !important;
            }
            section.content-body {
                padding: 10px !important;
            }
            #loaderspan img {
                width: 40px;
                height: 40px;
                margin: auto;
                top: 0;
                display: block;
                right: 0;
                bottom: 0;
                left: 0;
                /*position: absolute;*/
            }
            #success_submit {
                color: #179717B3;
                text-align: center;
                font-weight: bold;
                font-size: 15px;
                padding: 1% 0;
                display: none;
            }
            
        </style>
    </head>


    <body>
        <section class="body">

            <!-- start: header -->
            <header class="header">
                <div class="logo-container">
                    <a href="../" class="logo">
                        <h2 style="margin-top: 0 !important;">WHUC</h2>
                    </a>
                    <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <!-- start: search & user box -->
                <div class="header-right">

                    <div id="userbox" class="userbox">
                        <a href="#" data-toggle="dropdown">
                            <figure class="profile-picture">
                                <img src="<?= base_url('assets/images/!logged-user.jpg'); ?>" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                            </figure>
                            <div class="profile-info" data-lock-name="<?= $this->session->userdata('session_data')['name']; ?>" data-lock-email="<?= $this->session->userdata('session_data')['email']; ?>">
                                <span class="name"><?= $this->session->userdata('session_data')['name']; ?></span>
                                <span class="role">administrator</span>
                            </div>
                            <i class="fa custom-caret"></i>
                        </a>

                        <div class="dropdown-menu">
                            <ul class="list-unstyled">
                                <li class="divider"></li>
                                <li>
                                    <a role="menuitem" tabindex="-1" href="<?= base_url('admin/loggedOut/'); ?>"><i class="fa fa-power-off"></i> Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end: search & user box -->
            </header>
            <!-- end: header -->

            <div class="inner-wrapper">
                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">
                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Menu
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">
                                <ul class="nav nav-main">
                                    <li>
                                        <a href="<?= base_url('admin/dashboard'); ?>">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>
                                    </li> 
                                    <li class=" <?php if ($method == "userListing") echo 'nav-active'; ?>">
                                        <a href="<?= base_url('admin/userListing'); ?>">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <span>User Listing</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </aside>
                <!-- end: sidebar -->