<?php include SITE_ROOT . 'views/header.php'; ?>
<section id="page-title">

    <div class="container clearfix">
        <h1>Welcome</h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Welcome</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div style="max-width: 600px;" class="divcenter clearfix">
                <div class="col_full">
                    <h2 style="font-weight: 100; font-size: 26px; "><i class="glyphicon glyphicon-eye-open"></i>Welcome to Amadeus Travel Agency program</h2>
                    <div>
                        New to the program? <a href="<?=base_url('user/question');?>" class="btn btn-primary">Start Here</a> to fill short questionnaire.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include SITE_ROOT . 'views/footer.php' ?>
