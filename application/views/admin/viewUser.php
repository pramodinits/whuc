<?php
include SITE_ROOT . 'views/adminHeader.php';
?>
<section role="main" class="content-body">
    <header class="page-header">
<!--        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
        </div>-->

        <h2 class="panel-title">User Detail</h2>
    </header>
    <header class="panel-heading row">
            <div class="col-md-12 text-right">
                <a href="<?= $redirecturl; ?>">
                            <button class="btn btn-primary" id="sbmt">
                                Back
                            </button>
                        </a>
            </div>
        </header>
    <div class="panel-body">
        <table class="table table-striped">
            <tr>
                <td>Participate In</td>
                <td>:</td>
                <td><?= $userDetail['participate_in']; ?></td>
            </tr>
            <tr>
                <td>Participate As</td>
                <td>:</td>
                <td><?= $userDetail['participate_as']; ?></td>
            </tr>
            <tr>
                <td>Name Of Company</td>
                <td>:</td>
                <td><?= $userDetail['company_name']; ?></td>
            </tr>
            <tr>
                <td>Contact Full Name</td>
                <td>:</td>
                <td><?= $userDetail['contact_name']; ?></td>
            </tr>
            <tr>
                <td>Job Title</td>
                <td>:</td>
                <td><?= $userDetail['job_title']; ?></td>
            </tr>
            <tr>
                <td>Office Tel</td>
                <td>:</td>
                <td><?= $userDetail['office_telephone'] ? $userDetail['office_telephone'] : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Mobile / Cellphone</td>
                <td>:</td>
                <td><?= $userDetail['mobile_no']; ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><?= $userDetail['email']; ?></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>:</td>
                <td><?= $userDetail['gender'] = 1 ? "Male" : "Female"; ?></td>
            </tr>
            <tr>
                <td>Nature Of Company Business</td>
                <td>:</td>
                <td><?= $userDetail['company_business_nature']; ?></td>
            </tr>
            <tr>
                <td>Address Line 1</td>
                <td>:</td>
                <td><?= $userDetail['address_line1']; ?></td>
            </tr>
            <tr>
                <td>Address Line 2</td>
                <td>:</td>
                <td><?= $userDetail['address_line2'] ? $userDetail['address_line2'] : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Town/City</td>
                <td>:</td>
                <td><?= $userDetail['city']; ?></td>
            </tr>
            <tr>
                <td>State/Provinces</td>
                <td>:</td>
                <td><?= $userDetail['state'] ? $userDetail['state'] : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Postal Code</td>
                <td>:</td>
                <td><?= $userDetail['postal_code'] ? $userDetail['postal_code'] : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Country</td>
                <td>:</td>
                <td><?= $userDetail['country']; ?></td>
            </tr>
            <tr>
                <td>Nationality</td>
                <td>:</td>
                <td><?= $userDetail['nationality']; ?></td>
            </tr>
            <tr>
                <td>How many Hajj Pilgrims Do You Take Annually</td>
                <td>:</td>
                <td><?= $userDetail['hajj_pilgrims_annually'] ? $userDetail['hajj_pilgrims_annually'] : 0; ?></td>
            </tr>
            <tr>
                <td>How Many Umrah Pilgrims Do You Take Annually?</td>
                <td>:</td>
                <td><?= $userDetail['umrah_pilgrims_annually'] ? $userDetail['umrah_pilgrims_annually'] : 0; ?></td>
            </tr>
            <tr>
                <td>Would You Like to become a member of the World Hajj Umrah Organisers Association?</td>
                <td>:</td>
                <td>
                    <?php
                    if ($userDetail['hajj_umrah_organisers_assocciation_membership'] == 1){
                        $value = "Yes";
                    } else {
                        $value = "No";
                    }
                    ?>
                <?= $userDetail['hajj_umrah_organisers_assocciation_membership'] ? $value : "N/A"; ?>
                </td>
            </tr>
            <tr>
                <td>Would You Like To Subscribe to the Hajj People Membership Program?</td>
                <td>:</td>
                <td>
                    <?php
                    if ($userDetail['hajj_people_membership'] == 1){
                        $value = "Yes";
                    } else {
                        $value = "No";
                    }
                    ?>
                    <?= $userDetail['hajj_people_membership'] ? $value : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Would You Like to become a member of the British Hajj and Umrah Council?</td>
                <td>:</td>
                <td>
                    <?php
                    if ($userDetail['british_hajj_umrah_council_membership'] == 1){
                        $value = "Yes";
                    } else {
                        $value = "No";
                    }
                    ?>
                    <?= $userDetail['british_hajj_umrah_council_membership'] ? $value : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Would You Like to become a member of the National Pilgrimage Organisers Association?</td>
                <td>:</td>
                <td>
                    <?php
                    if ($userDetail['national_pilgrimage_organisers_association'] == 1){
                        $value = "Yes";
                    } else {
                        $value = "No";
                    }
                    ?>
                    <?= $userDetail['national_pilgrimage_organisers_association'] ? $value : "N/A"; ?></td>
            </tr>
            
            <tr>
                <td>Social Media ID - Facebook</td>
                <td>:</td>
                <td><?= $userDetail['social_id_fb'] ? $userDetail['social_id_fb'] : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Social Media ID - Twitter</td>
                <td>:</td>
                <td><?= $userDetail['social_id_twitter'] ? $userDetail['social_id_twitter'] : "N/A"; ?></td>
            </tr>
            <tr>
                <td>Social Media ID - Instagram</td>
                <td>:</td>
                <td><?= $userDetail['social_id_instagram'] ? $userDetail['social_id_instagram'] : "N/a"; ?></td>
            </tr>
            <tr>
                <td>Registration Date</td>
                <td>:</td>
                <td><?= date('d-m-Y h:i:s', strtotime($userDetail['add_date'])); ?></td>
            </tr>
        </table>
    </div>
</section>
<?php
include SITE_ROOT . 'views/adminFooter.php';
?>