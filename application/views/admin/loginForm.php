<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<?php include SITE_ROOT . 'views/header.php'; ?>
<style>
    .captcha_input {
        float: left;
        margin-right: 14px;
        width: 62%;
    }
    @media (max-width: 500px){
        .captcha_input {
            width: 100% !important;
            margin-right: 0 !important;
            margin-bottom: 10px !important;
        }
    }
    .rand1, .rand2
    {
        padding: 13px;
        background-color: #00a9e0;
        float: left;
        font-weight: bold;
        border-radius: 50%;
    }
    .plus, .equalto
    {
        padding: 16px 10px;
        float: left;
    }
    .re
    {
        padding:5px;
        float:left; 
        margin-right: 15px;
        cursor:pointer;
    }
    #login-form-submit {
        color: #dedbdb;
        font-weight: bold;
    }
</style>

<section id="content" style="min-height: 440px;">

    <div class="content-wrap">

        <div class="container clearfix">

            <div id="msg" class="btn btn-block"></div>
            <div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">

                <div class="acctitle" id="login_div_icon"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>Login to your Account</div>
                <div class="acc_content clearfix" id="login_div">
                    <form id="login-form" name="login-form" class="nobottommargin" autocomplete="off" action="<?= base_url('admin/setLogin'); ?>" method='post' onsubmit="return AsyncUpload.submitForm(this, loginformvalidation, logincalbackFun);">

                        <div class="col_full">
                            <label for="login-form-username">Username:</label>
                            <input type="text" id="login-form-username" readonly  
                                   onfocus="this.removeAttribute('readonly');" name="username" value="" autocomplete="off" class="form-control" required/>
                        </div>

                        <div class="col_full">
                            <label for="login-form-password">Password:</label>
                            <input type="password" id="login-form-password" readonly  
                                   onfocus="this.removeAttribute('readonly');"name="password" value="" autocomplete="off" class="form-control" required/>
                        </div>
                        <?php if (@$error) { ?>
                            <div class="col_full" style="color:orange;">

                                <div class="alert alert-error">      
                                    <?php echo @$error ?>
                                </div>

                            </div>
                        <?php } ?>
                        <div class="col_full nobottommargin">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <button type="submit" class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                                    <!--<a href="<?= base_url('user/forgotPassword/'); ?>" class="fright">Forgot Password?</a>-->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section><!-- #content end -->

<?php include SITE_ROOT . 'views/footer.php' ?>
<style>
    .regbut{
        color: #FFF;
        background-color: #ce0b59 !important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $('.phone_us').mask('999999999999999999999');
    });
    function loginformvalidation() {
        $("#login-form-username").removeAttr('readonly');
        $("#login-form-password").removeAttr('readonly');
        var validator = $("#login-form").validate({
            rules: {
                "username": {
                    required: true
                },
                "password": {
                    required: true
                },
            },
            messages: {
                "username": {
                    required: "This field is required."
                },
                "password": {
                    required: "This field is required."
                },
            }
        });
        var x = validator.form();
        if (x) {
            $("#login-form-submit").prop('disabled', true);
            return true;
        } else {
            $("#login-form-submit").prop('disabled', false);
            return false;
        }
    }
    function logincalbackFun(res) {
        var data = JSON.parse(res);
        if (data.hasError == true) {
            $("#login-form-submit").prop('disabled', false);
            $("#msg").show().html("<font color='red'>" + data.errormessage + "</font>");
            return false;
        } else {
            window.location.href = "" + data.redirecturl + "";
        }

    }

</script>