<?php
include SITE_ROOT . 'views/header.php';
?>
<style>
    .col_one_sixth {
        margin-right: 2% !important;
    }
    .col_full {
        margin-left: 2%;
        margin-bottom: 10px !important;
    }
    .col_one_fourth {
        text-align: center;
    }
    .col_one_sixth img{
        width: 100%;
        margin-top: 5%;
    }
    .footerlogo-text {
        font-size: 1em;
        color: darkgray;
    }
    .mt-4 {
        margin-top: 4%;
    }
</style>
<!-- Content ============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row">
                <div class="col_full header-location">
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">Arabia</div>
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">Africa</div>
                    <div class="col_one_sixth text-uppercase" style="color: #5d1a6e;">Europe</div>
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">Asia</div>
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">South-east asia</div>
                </div>
            </div>

            <div class="col_one_fourth">
                <!--<a href="<?= base_url('user/registration'); ?>">-->
                <a href="<?= base_url('user/exporegistration'); ?>">
                    <img src="<?= base_url('images/image1.png'); ?>">
                    <button class="btn button" value="register">register</button>
                </a>
            </div>

            <div class="col_one_fourth">
                <!--<a href="<?= base_url('user/registrationForum'); ?>">-->
                <a href="<?= base_url('user/whucforum'); ?>">
                    <img src="<?= base_url('images/image2.png'); ?>">
                    <button class="btn button">register</button>
                </a>
            </div>

            <div class="col_one_fourth">
                <!--<a href="<?= base_url('user/registrationDixie'); ?>">-->
                <a href="<?= base_url('user/dixieregistration'); ?>">
                    <img src="<?= base_url('images/image3.png'); ?>">
                    <button class="btn button">register</button>
                </a>
            </div>

            <div class="col_one_fourth col_last">
                <!--<a href="<?= base_url('user/registrationAwards'); ?>">-->
                <a href="<?= base_url('user/awardsregistration'); ?>">
                    <img src="<?= base_url('images/image4.png'); ?>">
                    <button class="btn button">register</button>
                </a>
            </div>

            <div class="clearfix"></div>
            <div class="row mt-4 brands-logo">
                <div class="col-md-6 col-xs-12">
                    <!--<div class="col_one_sixth" style="width: 30%; margin-right: 10% !important;">
                        <div class="text-center footerlogo-text">Hosted by</div>
                        <img src="<?= base_url('images/footer-logo1.png'); ?>">
                    </div>-->
                    <div class="col_one_sixth" style="width: 30%; margin-right: 10% !important;">
                        <div class="text-center footerlogo-text">Organised by</div>
                        <img src="<?= base_url('images/footer-logo2.png'); ?>">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="col_one_sixth pull-right" style="width: 40%;">
                        <div class="text-right footerlogo-text">In association with</div>
                        <img class="pull-right" onclick="showModal()" style="cursor:pointer;" src="<?= base_url('images/hajj.png'); ?>">
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>			
</section><!-- #content end -->
<!---Modal-->
<div class="modal fade col-md-12" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <img src="<?= base_url('images/Guest.jpg'); ?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!---Modal-->
<?php include SITE_ROOT . 'views/footer.php' ?>