<?php
include SITE_ROOT . 'views/header.php';
?>
<style>
    .col_one_sixth {
        margin-right: 2% !important;
    }
    .col_full {
        margin-left: 2%;
        margin-bottom: 10px !important;
    }
    .col_one_fourth {
        text-align: center;
    }
    .col_one_sixth img{
        width: 100%;
margin-top: 5%;
    }
    .footerlogo-text {
        font-size: 1em;
        color: darkgray;
    }
    .mt-4 {
        margin-top: 4%;
    }
</style>
<!-- Content ============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row">
                <div class="col_full header-location">
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">Arabia</div>
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">Africa</div>
                    <div class="col_one_sixth text-uppercase" style="color: #5d1a6e;">Europe</div>
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">Asia</div>
                    <div class="col_one_sixth text-uppercase" style="color: #969696;">South-east asia</div>
                </div>
            </div>

            <div class="col_one_fourth">
                <img src="<?= base_url('images/image1.png'); ?>">
                <a href="<?= base_url('user/registration'); ?>">
                <button class="btn button" value="register">register</button>
                </a>
            </div>

            <div class="col_one_fourth">
                <img src="<?= base_url('images/image2.png'); ?>">
                <a href="<?= base_url('user/registrationForum'); ?>">
                <button class="btn button">register</button>
                </a>
            </div>

            <div class="col_one_fourth">
                <img src="<?= base_url('images/image3.png'); ?>">
                <a href="<?= base_url('user/registrationDixie'); ?>">
                <button class="btn button">register</button>
                </a>
            </div>

            <div class="col_one_fourth col_last">
                <img src="<?= base_url('images/image4.png'); ?>">
                <a href="<?= base_url('user/registrationAwards'); ?>">
                    <button class="btn button">register</button>
                </a>
            </div>
            
            <div class="clearfix"></div>
            <div class="row mt-4 brands-logo">
                <div class="col-md-6 col-xs-12">
                    <div class="col_one_sixth" style="width: 30%; margin-right: 10% !important;">
                        <div class="text-center footerlogo-text">Hosted by</div>
                        <img src="<?= base_url('images/footer-logo1.png'); ?>">
                    </div>
                    <div class="col_one_sixth" style="width: 30%; margin-right: 10% !important;">
                        <div class="text-center footerlogo-text">Organised by</div>
                        <img src="<?= base_url('images/footer-logo2.png'); ?>">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="col_one_sixth pull-right" style="width: 30%;">
                        <div class="text-right footerlogo-text">In association with</div>
                        <img class="pull-right" src="<?= base_url('images/vision-2030-logo.png'); ?>">
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>			
</section><!-- #content end -->
<?php include SITE_ROOT . 'views/footer.php' ?>