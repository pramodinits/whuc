<section role="main" class="content-body">
    <header class="page-header">
        <h2 class="panel-title">Users</h2>
    </header>
    <!--form section-->
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Edit User</h2>
        </header>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" name="regForm" id="regForm" action="<?= base_url('admin/editUser/'); ?>" method="post" enctype="multipart/form-data" onsubmit="return AsyncUpload.submitForm(this, formvalidation, calbackFun);">
                <input type="hidden" name="reg_id" value="<?php echo $userDetail['reg_id']; ?>" id="reg_id" />
                <input type="hidden" name="reg[flag]" value="<?php echo $userDetail['flag']; ?>" id="flag" />
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">
                        Participate In<span>*</span>
                    </label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Europe" name="reg[participate_in]" value="Europe" <?= $userDetail['participate_in'] == "Europe" ? 'checked' : "" ?> /> 
                            <label for="Europe">Europe</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Africa" name="reg[participate_in]" value="Africa" <?= $userDetail['participate_in'] == "Africa" ? 'checked' : "" ?> /> 
                            <label for="Africa">Africa</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="South_Asia" name="reg[participate_in]" value="South Asia" <?= $userDetail['participate_in'] == "South Asia" ? 'checked' : "" ?> /> 
                            <label for="South_Asia">South Asia</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Arabia" name="reg[participate_in]" value="Arabia" <?= $userDetail['participate_in'] == "Arabia" ? 'checked' : "" ?> /> 
                            <label for="Arabia">Arabia</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="South_East_Asia" name="reg[participate_in]" value="South East Asia" <?= $userDetail['participate_in'] == "South East Asia" ? 'checked' : "" ?> /> 
                            <label for="South_East_Asia">South East Asia</label>
                        </div>
                        <label for="participate_in" generated="true" class="error" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="">
                        Participate As<span>*</span>
                    </label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Visitor" name="reg[participate_as]" value="Visitor" <?= $userDetail['participate_as'] == "Visitor" ? 'checked' : "" ?> /> 
                            <label for="Visitor">Visitor</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Exhibitor" name="reg[participate_as]" value="Exhibitor" <?= $userDetail['participate_as'] == "Exhibitor" ? 'checked' : "" ?> /> 
                            <label for="Exhibitor">Exhibitor</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Sponsor" name="reg[participate_as]" value="Sponsor" <?= $userDetail['participate_as'] == "Sponsor" ? 'checked' : "" ?> /> 
                            <label for="Sponsor">Sponsor</label>
                        </div>
                        <label for="participate_as" generated="true" class="error" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Name Of Company<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[company_name]" value="<?php echo $userDetail['company_name'] ?>" id="company_name" class="form-control" />
                        <label for="company_name" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Contact Full Name<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[contact_name]" value="<?php echo $userDetail['contact_name'] ?>" id="contact_name" class="form-control" />
                        <label for="contact_name" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Job Title<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[job_title]" value="<?php echo $userDetail['job_title'] ?>" id="job_title" class="form-control" />
                        <label for="job_title" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Office Tel<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[office_telephone]" value="<?php echo $userDetail['office_telephone'] ?>" id="office_telephone" class="form-control" />
                        <label for="office_telephone" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Mobile / Cellphone<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[mobile_no]" value="<?php echo $userDetail['mobile_no'] ?>" id="mobile_no" class="form-control" />
                        <label for="mobile_no" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Email<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[email]" value="<?php echo $userDetail['email'] ?>" id="email" class="form-control" />
                        <label for="email" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Gender<span>*</span></label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Male" name="reg[gender]" value="1>" <?= $userDetail['gender'] == "1" ? 'checked' : "" ?> /> 
                            <label for="Male">Male</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Female" name="reg[gender]" value="2" <?= $userDetail['gender'] == "2" ? 'checked' : "" ?> /> 
                            <label for="Female">Female</label>
                        </div>
                        <label for="gender" generated="true" class="error" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Date of Birth<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="date" name="birth_date" value="<?php echo $userDetail['dob']; ?>" id="birth_date" class="form-control" />
                        <span class="text-sm">ex. dd/mm/yyyy</span><br>
                        <label for="birth_date" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Address Line1<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[address_line1]" value="<?php echo $userDetail['address_line1'] ?>" id="address_line1" class="form-control" />
                        <label for="address_line1" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Address Line2<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[address_line2]" value="<?php echo $userDetail['address_line2'] ?>" id="address_line2" class="form-control" />
                        <label for="address_line2" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Town/City<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[city]" value="<?php echo $userDetail['city'] ?>" id="city" class="form-control" />
                        <label for="city" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">State<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[state]" value="<?php echo $userDetail['state'] ?>" id="state" class="form-control" />
                        <label for="state" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Postal Code<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[postal_code]" value="<?php echo $userDetail['postal_code'] ?>" id="postal_code" class="form-control" />
                        <label for="postal_code" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Country<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[country]" value="<?php echo $userDetail['country'] ?>" id="country" class="form-control" />
                        <label for="country" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Nationality<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[nationality]" value="<?php echo $userDetail['nationality'] ?>" id="nationality" class="form-control" />
                        <label for="nationality" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Passport Number<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[passport_no]" value="<?php echo $userDetail['passport_no'] ?>" id="passport_no" class="form-control" />
                        <label for="passport_no" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Nature Of Company Business<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[company_business_nature]" value="<?php echo $userDetail['company_business_nature'] ?>" id="company_business_nature" class="form-control" />
                        <label for="company_business_nature" generated="true" class="error text-red" style="display:none;"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">How many Hajj Pilgrims Do You Take Annually?<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="hajj_pilgrims_annually" value="<?php echo $userDetail['hajj_pilgrims_annually'] ?>" id="hajj_pilgrims_annually" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">How Many Umrah Pilgrims Do You Take Annually?<span>*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="umrah_pilgrims_annually" value="<?php echo $userDetail['umrah_pilgrims_annually'] ?>" id="umrah_pilgrims_annually" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Would You Like to become a member of the World Hajj Umrah Organisers Association?<span>*</span></label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_umrah_organisers_yes" name="reg[hajj_umrah_organisers_assocciation_membership]" value="1" 
                                   <?= $userDetail['hajj_umrah_organisers_assocciation_membership'] == "1" ? 'checked' : "" ?> /> 
                            <label for="hajj_umrah_organisers_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_umrah_organisers_no" name="reg[hajj_umrah_organisers_assocciation_membership]" value="2" 
                                   <?= $userDetail['hajj_umrah_organisers_assocciation_membership'] == "2" ? 'checked' : "" ?> /> 
                            <label for="hajj_umrah_organisers_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Would You Like to become a member of the British Hajj and Umrah Council?<span>*</span></label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="british_hajj_umrah_yes" name="reg[british_hajj_umrah_council_membership]" value="1" 
                                   <?= $userDetail['british_hajj_umrah_council_membership'] == "1" ? 'checked' : "" ?> /> 
                            <label for="british_hajj_umrah_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="british_hajj_umrah_no" name="reg[british_hajj_umrah_council_membership]" value="2" 
                                   <?= $userDetail['british_hajj_umrah_council_membership'] == "2" ? 'checked' : "" ?> /> 
                            <label for="british_hajj_umrah_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        Would You Like to become a member of the National Pilgrimage Organisers Association?</label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="national_pilgrimage_yes" name="reg[national_pilgrimage_organisers_association]" value="1" 
                                   <?= $userDetail['national_pilgrimage_organisers_association'] == "1" ? 'checked' : "" ?> /> 
                            <label for="national_pilgrimage_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="national_pilgrimage_no" name="reg[national_pilgrimage_organisers_association]" value="2" 
                                   <?= $userDetail['national_pilgrimage_organisers_association'] == "2" ? 'checked' : "" ?> /> 
                            <label for="national_pilgrimage_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        Would You Like To Subscribe to the Hajj People Membership Program?</label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_people_membership_yes" name="reg[hajj_people_membership]" value="1" 
                                   <?= $userDetail['hajj_people_membership'] == "1" ? 'checked' : "" ?> /> 
                            <label for="hajj_people_membership_yes">Yes</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="hajj_people_membership_no" name="reg[hajj_people_membership]" value="2" 
                                   <?= $userDetail['hajj_people_membership'] == "2" ? 'checked' : "" ?> /> 
                            <label for="hajj_people_membership_no">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        Preferred Payment Option</label>
                    <div class="col-md-6">
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Bank_Transfer" name="reg[payment_option]" value="Bank Transfer" 
                                   <?= $userDetail['payment_option'] == "Bank Transfer" ? 'checked' : "" ?> /> 
                            <label for="Bank_Transfer">Bank Transfer</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Credit_Card" name="reg[payment_option]" value="Credit Card" 
                                   <?= $userDetail['payment_option'] == "Credit Card" ? 'checked' : "" ?> /> 
                            <label for="Credit_Card">Credit Card</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Paypal" name="reg[payment_option]" value="Paypal" 
                                   <?= $userDetail['payment_option'] == "Paypal" ? 'checked' : "" ?> /> 
                            <label for="Paypal">Paypal</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Western_Union" name="reg[payment_option]" value="Western Union" 
                                   <?= $userDetail['payment_option'] == "Western Union" ? 'checked' : "" ?> /> 
                            <label for="Western_Union">Western Union</label>
                        </div>
                        <div class="radio-custom radio-primary">
                            <input type="radio" id="Except_from_Payment" name="reg[payment_option]" value="Except from Payment" 
                                   <?= $userDetail['payment_option'] == "Except from Payment" ? 'checked' : "" ?> /> 
                            <label for="Except_from_Payment">Except from Payment</label>
                        </div>
                    </div>
                </div>
                <h3>Social Media ID</h3>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        <img src="<?= base_url("images/f1.png"); ?>" style="width: 40px; height: 40px;margin-top: -5%;">
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[social_id_fb]" value="<?php echo $userDetail['social_id_fb'] ?>" id="social_id_fb" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        <img src="<?= base_url("images/t1.png"); ?>" style="width: 40px; height: 40px;margin-top: -5%;">
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[social_id_twitter]" value="<?php echo $userDetail['social_id_twitter'] ?>" id="social_id_twitter" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">
                        <img src="<?= base_url("images/i1.png"); ?>" style="width: 40px; height: 40px;margin-top: -5%;">
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="reg[social_id_instagram]" value="<?php echo $userDetail['social_id_instagram'] ?>" id="social_id_instagram" class="form-control" />
                    </div>
                </div>

                <div class="alert-error" id="error_server" style="padding: 0 0 5px 0; display: none; text-align: center;">

                </div>
                <div class="alert-error" id="error_server" style="padding: 0 0 5px 0; display: none; text-align: center;">

                </div>
                <div id="loaderspan" style="display:none;">
                    <img src="<?= base_url('images/loader1.gif'); ?>">
                </div>
                <div class="uline" id="success_submit">
                    Your inquiry was updated successfully.
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="sbmt"></label>
                    <div class="col-md-9">
                        <button class="btn btn-primary" id="sbmt">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>