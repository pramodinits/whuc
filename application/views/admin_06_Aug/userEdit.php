<?php
include SITE_ROOT . 'views/adminHeader.php';

//set template as flag
if ($userDetail['flag'] == 1) {
    include SITE_ROOT . 'views/admin/editIslamicTourism.php';
} else if ($userDetail['flag'] == 2) {
    include SITE_ROOT . 'views/admin/editForum.php';
} else if ($userDetail['flag'] == 3) {
    include SITE_ROOT . 'views/admin/editDixie.php';
} else if ($userDetail['flag'] == 4) {
    include SITE_ROOT . 'views/admin/editAwards.php';
}
?>
<script type="text/javascript">

    function formvalidation() {
        var validator = $("#regForm").validate({
            rules: {
                "reg[participate_in]": {
                    required: true
                },
                "reg[participate_as]": {
                    required: true
                },
                "reg[company_name]": {
                    required: true
                },
                "reg[contact_name]": {
                    required: true
                },
                "reg[job_title]": {
                    required: true
                },
                "reg[office_telephone]": {
                    required: true,
                    digits: true
                },
                "reg[mobile_no]": {
                    required: true,
                    digits: true
                },
                "reg[email]": {
                    required: true,
                    email: true
                },
                "reg[gender]": {
                    required: true
                },
                "birth_date": {
                    required: true
                },
                "reg[company_business_nature]": {
                    required: true
                },
                "reg[passport_no]": {
                    required: true
                },
                "reg[nationality]": {
                    required: true
                },
                "reg[country]": {
                    required: true
                },
                "reg[postal_code]": {
                    required: true,
                    digits: true
                },
                "reg[state]": {
                    required: true
                },
                "reg[city]": {
                    required: true
                },
                "reg[address_line2]": {
                    required: true
                },
                "reg[address_line1]": {
                    required: true
                }
            },
            messages: {
                "reg[participate_in]": {
                    required: "This field is required."
                },
                "reg[participate_as]": {
                    required: "This field is required."
                },
                "reg[company_name]": {
                    required: "This field is required.",
                },
                "reg[contact_name]": {
                    required: "This field is required."
                },
                "reg[job_title]": {
                    required: "This field is required."
                },
                "reg[office_telephone]": {
                    required: "This field is required."
                },
                "reg[mobile_no]": {
                    required: "This field is required."
                },
                "reg[email]": {
                    required: "This field is required.",
                    email: "Enter an valid email."
                },
                "reg[gender]": {
                    required: "This field is required."
                },
                "birth_date": {
                    required: "This field is required."
                },
                "reg[company_business_nature]": {
                    required: "This field is required."
                },
                "reg[passport_no]": {
                    required: "This field is required."
                },
                "reg[nationality]": {
                    required: "This field is required."
                },
                "reg[country]": {
                    required: "This field is required."
                },
                "reg[postal_code]": {
                    required: "This field is required."
                },
                "reg[state]": {
                    required: "This field is required."
                },
                "reg[city]": {
                    required: "This field is required."
                },
                "reg[address_line2]": {
                    required: "This field is required."
                },
                "reg[address_line1]": {
                    required: "This field is required."
                }
            }
        });
        var x = validator.form();
        if (x) {
            $("#sbmt").prop('disabled', true);
            $('#loaderspan').show();
            return true;
        } else {
            $("#sbmt").prop('disabled', false);
            return false;
        }
    }
    function calbackFun(result) {
        $('#loaderspan').hide();
        var data = JSON.parse(result);
        if (data.hasError == true) {
            if (data.servermessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.servermessage + "</h4>");
                return false;
            } else if (data.methodmessage) {
                $("#error_server").show().html("<h4 class='text-danger'>" + data.methodmessage + "</h4>");
                return false;
            }
        } else {
            $("#sbmt").prop('disabled', false);
            $('#success_submit').show();
            setTimeout(function () {
                $('#success_submit').hide();
                window.location.href = "" + data.redirecturl + "";
            }, 5000);
            return false;
        }
    }
</script>
<?php
include SITE_ROOT . 'views/adminFooter.php';
?>

