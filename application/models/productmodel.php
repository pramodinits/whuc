<?php

class Productmodel extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function productListing($search = array()) {
        $data = array();
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('status = 1');
        $query = $this->db->get();
        $return = array();
        foreach ($query->result_array() as $row) {
            $return[$row['type']][] = $row; 
        }
        return $return;
    }
    function productRequested($id_user){
        $data = "";
        $sql = "SELECT id_product FROM product_request WHERE id_user = $id_user";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $data[]=$row['id_product'];
        }
        return $data;
    }
    
    function productDetail($productId) {
        $data = "";
        $sql = "SELECT id_product,name,description FROM product WHERE id_product = $productId";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
   

}
