<?php

class Faqmodel extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function faqListing($search = array()) {
        $data = array();
        $this->db->select('*');
        $this->db->from('faq');
        $this->db->where('status', 1);
        $this->db->order_by('id_faq', 'ASC');

        $query = $this->db->get();
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

}
