<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('adminmodel');
        $this->load->library('pagination');
        $this->load->helper('download');
        $this->load->library('form_validation');
        $this->load->helper('email');
    }

    public function setmysession() {
        $the_session = array("key1" => "value1", "key2" => "value2");
        $this->session->set_userdata($the_session);
        print "<pre>";
        $ss = $this->session->userdata();
        print_r($ss);
    }

    public function getmysession() {
        $ss = $this->session->userdata();
        print "<pre>";
        print_r($ss);
    }

    public function index() {

        $this->load->view("user/homePage");
    }

//static page

    public function privacy() {
        $this->load->view("user/privacy");
    }

    public function termsUse() {
        $this->load->view("user/termsUse");
    }

    public function cookies() {
        $this->load->view("user/cookies");
    }

    public function contactus() {
        $this->load->view("user/contactus");
    }

    public function aboutus() {
        $this->load->view("user/aboutus");
    }

    /**
     * For WHUC admin registration
     */
    public function registration() {
        $data = array();
        $data['logo'] = "images/reg-logo.png";
        $data['title'] = "HAJJ People Islamic Tourism ";
        $this->load->view("user/registrationIslamicTourism", $data);
    }

    public function registrationMultiple() {
        $data = array();
        $data['logo'] = "images/reg-logo.png";
        $data['title'] = "HAJJ People Islamic Tourism ";
        $this->load->view("user/registrationMultiple", $data);
    }
    
    public function exporegistration() {
        $data = array();
        $data['logo'] = "images/reg-logo.png";
        $data['title'] = "HAJJ People Expo ";
        $this->load->view("user/exporegistration", $data);
    }

    public function registrationForum() {
        //echo  exit();
        $data = array();
        $data['logo'] = "images/forum-reg-logo.png";
        $data['title'] = "WHUC Forum";
        $this->load->view("user/registrationForum", $data);
    }
    
    public function whucforum() {
        //echo  exit();
        $data = array();
        $data['logo'] = "images/forum-reg-logo.png";
        $data['title'] = "WHUC Forum";
        $this->load->view("user/whucforum", $data);
    }

    public function registrationDixie() {
        $data = array();
        $data['logo'] = "images/dixie.png";
        $data['title'] = "Dixie Queen";
        $this->load->view("user/registrationDixie", $data);
    }
    
    public function dixieregistration() {
        $data = array();
        $data['logo'] = "images/dixie.png";
        $data['title'] = "Dixie Queen";
        $this->load->view("user/dixieregistration", $data);
    }

    public function registrationAwards() {
        $data = array();
        $data['logo'] = "images/hajj.png";
        $data['title'] = "Hajj People Awards";
        $this->load->view("user/registrationAwards", $data);
    }
    
    public function awardsregistration() {
        $data = array();
        $data['logo'] = "images/hajj.png";
        $data['title'] = "Hajj People Awards";
        $this->load->view("user/awardsregistration", $data);
    }

    public function insertregUser() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $reg = $this->input->post('reg');
            $reg['umrah_pilgrims_annually'] = !empty($this->input->post('umrah_pilgrims_annually')) ? $this->input->post('umrah_pilgrims_annually') : 0;
            $reg['hajj_pilgrims_annually'] = !empty($this->input->post('hajj_pilgrims_annually')) ? $this->input->post('hajj_pilgrims_annually') : 0;

            //check email exist
            $cond = "flag =" . $reg['flag'];
            $record_detail = $this->adminmodel->detail("registration_user", "email", $reg['email'], $cond);
            if ($record_detail) {
                if ($record_detail['status'] == 1) {
                    $res['hasError'] = true;
                    $res['existsmessage'] = "Email exists";
                    echo json_encode($res);
                    exit;
                } else {
                    $res['hasError'] = true;
                    $res['existsmessage'] = "Email already registered. Kindly verify.";
                    echo json_encode($res);
                    exit;
                }
            }
            $reg['add_date'] = date("Y-m-d H:i:s");
            $reg['ip'] = $_SERVER['REMOTE_ADDR'];
            $multiflg = false;
            $ins_id = $this->adminmodel->insertData('registration_user', $reg);
            
            //insert multiple
            $expo = $this->input->post('expo');
            $forum = $this->input->post('forum');
            $dixie = $this->input->post('dixie');
            $awards = $this->input->post('awards');
            $rand = rand(1, 10);
            $uniquelink = md5($reg['email'] . $rand);
            if (@$expo['participate_in']) {
                $reg['flag'] = 1;
                $reg['participate_in'] = @$expo['participate_in'];
                $reg['participate_as'] = @$expo['participate_as'];
                $reg['email_verify'] = $uniquelink;
                $cond = "flag = 1";
                $record_detail = $this->adminmodel->detail("registration_user", "email", $reg['email'], $cond);
                if (!$record_detail) {
                    $multiflg = true;
                    $insid = $this->adminmodel->insertData('registration_user', $reg);
                }
            }
            if (@$forum['participate_in']) {
                $reg['flag'] = 2;
                $reg['participate_in'] = @$forum['participate_in'];
                $reg['participate_as'] = @$forum['participate_as'];
                $reg['email_verify'] = $uniquelink;
                $cond = "flag = 2";
                $record_detail = $this->adminmodel->detail("registration_user", "email", $reg['email'], $cond);
                if (!$record_detail) {
                    $multiflg = true;
                    $insid = $this->adminmodel->insertData('registration_user', $reg);
                }
            }
            if (@$this->input->post('flag3')) {
                $reg['flag'] = 3;
                $reg['participate_in'] = @$dixie['participate_in'];
                $reg['participate_as'] = @$dixie['participate_as'];
                $reg['email_verify'] = $uniquelink;
                $cond = "flag = 3";
                $record_detail = $this->adminmodel->detail("registration_user", "email", $reg['email'], $cond);
                if (!$record_detail) {
                    $multiflg = true;
                    $insid = $this->adminmodel->insertData('registration_user', $reg);
                }
            }
            if (@$this->input->post('flag4')) {
                $reg['flag'] = 4;
                $reg['participate_in'] = @$awards['participate_in'];
                $reg['participate_as'] = @$awards['participate_as'];
                $reg['email_verify'] = $uniquelink;
                $cond = "flag = 4";
                $record_detail = $this->adminmodel->detail("registration_user", "email", $reg['email'], $cond);
                if (!$record_detail) {
                    $multiflg = true;
                    $insid = $this->adminmodel->insertData('registration_user', $reg);
                }
            }
            //insert multiple
            
            //activation link
            if ($multiflg) {
                $upd = array();
                $upd['email_verify'] = $uniquelink;
                $upd_id = $this->adminmodel->updateData('registration_user', $upd, 'reg_id', $ins_id);
                
                $contentMail = 'Dear ' . $reg["contact_name"] . ', <br/><br/>'
                        . 'Thank you for registering with us.<br/>'
                        . 'Kindly <a href="' . base_url('emailMultiVerification/' . $uniquelink) . '"> click here </a> to complete your registration process. '
                        . '<br>'
                        . 'If click on the link is not working, you can go through it by copy & paste the URL.<br>'
                        . base_url('emailMultiVerification/' . $uniquelink) . "<br/><br/>";
                /* core email */
                $subject = "User Registration : Email Verification";
                $body = $contentMail;
                $to = $reg['email'];
                $cc = $adminEmail = $this->config->item('adminemail');
                $this->sendEmail($subject, $body, $to, $cc);

                $res['hasError'] = false;
                $res['redirecturl'] = base_url();
            } else {
                $upd = array();
                $upd['email_verify'] = md5($ins_id . $reg['flag'] . rand(1, 10));
                $upd_id = $this->adminmodel->updateData('registration_user', $upd, 'reg_id', $ins_id);

                $contentMail = 'Dear ' . $reg["contact_name"] . ', <br/><br/>'
                        . 'Thank you for registering with us.<br/>'
                        . 'Kindly <a href="' . base_url('emailVerification/' . $upd['email_verify']) . '"> click here </a> to complete your registration process. '
                        . '<br>'
                        . 'If click on the link is not working, you can go through it by copy & paste the URL.<br>'
                        . base_url('emailVerification/' . $upd['email_verify']) . "<br/><br/>";
                /* core email */
                $subject = "User Registration : Email Verification";
                $body = $contentMail;
                $to = $reg['email'];
                $cc = $adminEmail = $this->config->item('adminemail');
                $this->sendEmail($subject, $body, $to, $cc);

                $res['hasError'] = false;
                $res['redirecturl'] = base_url();
            }
        } else {
            $res['hasError'] = true;
            $res['methodmessage'] = "Internal Server Error";
        }
        echo json_encode($res);
        exit();
    }

    public function emailVerification($activation_key) {
        //check activation key in db
        $checkUSer = $upd = array();
        // if($activa)
        $checkUSer = $this->adminmodel->detail("registration_user", "email_verify", $activation_key);

        if ($checkUSer && ($activation_key != 1)) {
            if ($checkUSer['email_verify'] == 1) {
//                echo "<strong>Already activated</strong>";
//                header("refresh:2;url=" . base_url() . "");
                $this->session->set_flashdata('info', 'Already activated');
                redirect(base_url());
            } else {
                $upd['email_verify'] = 1;
                $upd_id = $this->adminmodel->updateData('registration_user', $upd, 'email_verify', $activation_key);

                $contentMail = 'Dear ' . $checkUSer["contact_name"] . ', <br/><br/>'
                        . 'Thank you for verifying your email.<br/>'
                        . 'Your registration process is completed.<br/>';
                /* core email */
                $subject = "User Registration Completed";
                $body = $contentMail;
                $to = $checkUSer['email'];
                $cc = $adminEmail = $this->config->item('adminemail');
                $this->sendEmail($subject, $body, $to, $cc);
                $this->session->set_flashdata('success', 'Successfully activated');
                redirect(base_url());
//                return $this->load->view('user/homePage');
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid activation link');
            redirect(base_url());
//            echo '<p class="alert alert-danger"><strong>Invalid activation link</strong></p>';
//            header("refresh:2;url=" . base_url() . "");
        }
    }

    public function emailMultiVerification($activation_key) {
        //check activation key in db
        $checkUSer = $upd = array();
        // if($activa)
        $checkUSer = $this->adminmodel->detail("registration_user", "email_verify", $activation_key);

        if ($checkUSer && ($activation_key != 1)) {
            if ($checkUSer['email_verify'] == 1) {
//                echo "<strong>Already activated</strong>";
//                header("refresh:2;url=" . base_url() . "");
                $this->session->set_flashdata('info', 'Already activated');
                redirect(base_url());
            } else {
                $upd['email_verify'] = 1;
                $upd_id = $this->adminmodel->updateData('registration_user', $upd, 'email_verify', $activation_key);

                $contentMail = 'Dear ' . $checkUSer["contact_name"] . ', <br/><br/>'
                        . 'Thank you for verifying your email.<br/>'
                        . 'Your registration process is completed.<br/>';
                /* core email */
                $subject = "User Registration Completed";
                $body = $contentMail;
                $to = $checkUSer['email'];
                $cc = $adminEmail = $this->config->item('adminemail');
                $this->sendEmail($subject, $body, $to, $cc);
                $this->session->set_flashdata('success', 'Successfully activated');
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid activation link');
            redirect(base_url());
        }
    }

    public function testEmail() {
        $subject = "Test Test ";
        $body = "Body";
        $to = "sahoo.purusottama@gmail.com";
        $cc = "sales@skycobalt.com,smore@skycobalt.com";
        echo $this->sendEmail($subject, $body, $to, $cc);
    }

    public function sendEmail($subject, $body, $to, $cc, $filepath = null) {
        $config = Array(
            'protocol' => 'smtp',
            //'smtp_host' => 'lnx25.securehostdns.com',
            // 'smtp_host' => 'ssl://smtp.zoho.com',
            'smtp_host' => 'metro706.hostmetro.com',
            'smtp_port' => 587,
            //'smtp_port' => 465,
            'smtp_user' => 'smtp@skycobalt.com',
            'smtp_pass' => 'smtp@143',
            'mailtype' => 'html',
                //'smtp_user' => 'alert@iliftserviceapp.com',
                //'smtp_pass' => 'Ali#241@dkj'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('event@hajjpeople.com', 'Info');
        if ($to) {
            $this->email->to($to);
        }

        if ($cc) {
            $this->email->cc($cc);
        }
        if ($filepath) {
            $this->email->attach($filepath);
        }

        $this->email->subject($subject);
        $this->email->message($body);
        return $this->email->send();

        return true;
//        if (!$this->email->send()) {
//            show_error($this->email->print_debugger());
//        } else {
//            echo 'Your e-mail has been sent!';
//        }
    }

}
