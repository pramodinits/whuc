<?php
/**
 * This example shows sending a message using PHP's mail() function.
 */

require 'PHPMailer-master/PHPMailerAutoload.php';
$que = array
    (
    'IATA' => yes,
    'staff' => 139,
    'tickets' => 180,
    'GDS' => no,
    'own_tickets' => no,
    'ticket_partner' => no,
    'fulfillment' => Array
        (
        'flg' => no,
        'agency' => null,
    ),
    'hotels' => no,
    'domain' => no,
    'act_solution' => no,
    'question_website' => no
);
$toemail = 'ranjita.sahoo@thoughtspheres.com';
$name = 'ranjita';
$frm_email = 'run09.cet@gmail.com';
$subject = 'mail testing';
$name = "ranjita";
$email = "ranjitamayeesahoo@gmail.com";
$phone = "7873387489";
$agency_name = "test agency";
$address = "bbser";
$city = "bbsr";
$country = "india";
$message = "<html>
<body>
<div><b>User Detail:</b></div>
<table width='50%' cellpadding='0' cellspacing='0' border='0' id='background-table' >
    <tr>
        <td>Name : </td>
        <td>" . $name . "</td>
    </tr>
    <tr>
        <td>Email : </td>
        <td>" . $email . "</td>
    </tr>
    <tr>
        <td>Phone : </td>
        <td>" . $phone . "</td>
    </tr>
    <tr>
        <td>Agency Name : </td>
        <td>" . $agency_name . "</td>
    </tr>
    <tr>
        <td>Address : </td>
        <td>" . $address . "," . $city . "," . $country . "</td>
    </tr>
</table>
<div><b>Questionnaire Detail:</b></div>
<table width='50%' >
            <tr>
                <td>1.Are you IATA certified travel agency?</td>
                <td>:</td>
                <td>" . $que['IATA'] . "</td>
            </tr>
            <tr>
                <td>2.How many staff do you have?</td>
                <td>:</td>
                <td>" . $staff . "</td>
            </tr>
            <tr>
                <td>3.How many Air tickets do you sell/intend to sell per month?</td>
                <td>:</td>
                <td>" . $tickets . "</td>
            </tr>
            <tr>
                <td>4.Do you work with any GDS today?</td>
                <td>:</td>
                <td>" . $que['GDS'] . "</td>
            </tr>

            <tr>
                <td>5.Do you issue your own tickets?</td>
                <td>:</td>
                <td>" . $que['own_tickets'] . "</td>
            </tr>
            <tr>
                <td>6.Do you have current preferred ticketing partners?</td>
                <td>:</td>
                <td>" . $que['ticket_partner'] . "</td>
            </tr>
            <tr>
                <td>7. Would you like to have any of our fulfilment agencies?</td>
                <td>:</td>
                <td>" . $que['fulfillment']['flg'] . "</td>
            </tr>
            <tr>
                    <td>Agencies are</td>
                    <td>:</td>
                    <td>
                        <ul>" . $results . "</ul>
                    </td>
                </tr>
            <tr>
                <td>8.Do you sell / intend to sell hotels?</td>
                <td>:</td>
                <td>" . $que['hotels'] . "</td>
            </tr>
            <tr>
                <td>9.Do you have your own company e-mail/domain?</td>
                <td>:</td>
                <td>" . $que['domain'] . "</td>
            </tr>
            <tr>
                <td>10.Do you have an accounting solution?</td>
                <td>:</td>
                <td>" . $que['act_solution'] . "</td>
            </tr>
            <tr>
                <td>11.Do you have a website?</td>
                <td>:</td>
                <td>" . $que['question_website'] . "</td>
            </tr>
        </table>
</body>
</html>";

//Create a new PHPMailer instance
$mail = new PHPMailer;
//print_r($mail);exit;
//Set who the message is to be sent from
$mail->setFrom('ranjitamayeesahoo@gmail.com', 'Ranjita', 0);
//Set an alternative reply-to address
$mail->addReplyTo('ranjitamayeesahoo@gmail.com', 'Ranjita');
//Set who the message is to be sent to
$mail->addAddress($toemail, 'Ranjita');
//Set the subject line
$mail->Subject = 'PHPMailer mail function test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = $message;
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}